const express = require('express');
const mongoose = require('mongoose')
const session = require('express-session')
const cParser = require('cookie-parser')
const path = require('path')
const fs = require('fs')

require('dotenv').config()
const socket = require('socket.io')

// const cookies = requirejs('cookie.js')
const updateJsonFile = require('update-json-file')
var cryptorjs = require('cryptorjs')
// var myCryptor = new cryptorjs(process.env.CRYPTOR_PASS);

const app = express();
const http = require('http').Server(app)
var io = socket.listen(http);
mongoose.connect('mongodb://127.0.0.1:27017/ionico', { useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true})
.then(db => console.log("Database Connected Successfully"))
.catch(err => console.error("Houston, we have a problem here: \n"+err))

//Settings
app.set('port', process.env.PORT || 3000)

// cookie.set('room', 'asd')
// console.log(cookies)
updateJsonFile('u23r20nl1n3.txt', (dat) => {
    dat = []
    return dat;
})

//Middlewares
app.use(express.json());
app.use(cParser())
app.use(session({
    key: 'user_session_id',
    secret: '10n1c0s3cr3tc0d3',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: (60*60*1000)
    }
}))

// var address = require('address');
// Full Stack Function (IPv4, IPv6, MAC) - get virtual Address
// address(function (err, addrs) {
//     console.log(addrs.ip, addrs.ipv6, addrs.mac);
//   });
// Protocol by protocol method - get virtual Address
// address.ip();   // '192.168.0.2'
// address.ipv6(); // 'fe80::7aca:39ff:feb0:e67d'
// address.mac(function (err, addr) {
// console.log(addr); // '78:ca:39:b0:e6:7d'
// });

//Routes
const middleware = function (req, res, next) {
    if (!(req.session.user && req.cookies.user_session_id)) {
        res.redirect('/auth/login');
    } else {
        next();
    }    
};

//** COOKIES */
// app.get('/cookie-room', function (req, res) {
//     res.cookie(req.name, req.value).send('Cookie is set');
// });

// app.get('/', (req, res) => {
//     res.redirect('/auth/login')
// })

app.get('/', (req, res) => {
    if (req.session.user && req.cookies.user_session_id) {
        res.sendFile(path.join(__dirname, '/public/dashboard.html'));
        // res.sendFile(path.join(__dirname, '/public/sf.html'));
    } else {
        res.clearCookie('token');   
        res.cookie('token', null, {expire: 'Thu, 01 Jan 1970 00:00:01 GMT'})     
        res.redirect('/auth/login');
    }
})

app.get('/logout', (req, res) => {
    let json = JSON.parse(fs.readFileSync('u23r20nl1n3.txt'));
    res.clearCookie('token')
    let user = '';
    if(req.session.user){
        user = req.session.user.dni.dni
    }
    for (const key in json) {
        if(json[key].dni == user) {
            // cookies.remove('room')
            updateJsonFile('u23r20nl1n3.txt', (dat) => {
                dat.splice(key, 1) 
                return dat;
            })
            break;
        }
    }
    req.session.user = null
    res.redirect('/auth/login');
})

app.get('/vc/sc/:par', (req, res) => {res.redirect('/sc')})
app.get('/vc/sf/:par', (req, res) => {res.redirect('/sf')})
app.get('/sc', middleware,(req, res) => {res.sendFile(__dirname + '/public/sc.html')})
app.get('/sf', middleware,(req, res) => {res.sendFile(__dirname + '/public/sf.html')})
// app.get('/cookie', function (req, res) {
//     res.cookie('cookie_name', 'cookie_value').send('Cookie is set');
// });
app.get('/sc', (req, res) => {
    res.redirect('/sf');
})
// app.get('/print', (req, res) => {
//     res.sendFile(path.join(__dirname, 'public/programprint.html'));
// })
// // app.use('/sc', require('./src/Routes/sc'))
// app.use('/sf', require('./src/Routes/sf'))
// app.use('/api', require('./src/Routes/api'))
app.use('/auth', require('./src/Routes/auth'))

//404 default response
// app.use(function (req, res, next) {
//     res.status(404).send("Sorry we can't find that!")
// });

//Static Path
app.use(express.static(__dirname + '/public'))

//MAILER FUNCTION FILE
//require('./mailer')

//SOCKETS FILE IMPLEMENTATION
// require('./socketMain.js')(io)

//Server Listener
http.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`)
})