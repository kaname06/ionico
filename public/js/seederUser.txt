{
	"type": "admin",
	"license": {
		"lic": "158852853512",
		"details": [
			{
				"module": "SF",
				"items": ["0", "1", "2"]
			}
		]
	},
	"data": {
		"name": {
			"first": "Yeisson",
			"last": "Perez"
		},
		"dni": {
			"dType": "C.C",
			"dni": "1067964768"
		},
		"company": {
			"name": "Stark Industries",
			"nit": "98756734",
			"logo": "resources/img/stark12343",
			"license": "",
			"rols": [
				{
					"name": "Secretaria",
					"access": 
					[{
						"module": "SF",
						"items": [
							{
								"id": 0,
								"icon": "ifa fa fa-th",
								"item": "Farmacia",
								"subitems": [
									{
										"id":0,
										"url": "reception",
										"text": "Recepción"
									},
									{
										"id":1,
										"url": "dispensation",
										"text": "Dispensación"
									}
								]
							},
							{
								"id": 2,
								"icon": "ifa fa fa-list-alt",
								"item": "Listados",
								"subitems": [
									{
										"id":0,
										"url": "patient-list",
										"text": "Pacientes"
									},
									{
										"id":1,
										"url": "product-list",
										"text": "Productos"
									},
									{
										"id":2,
										"url": "dispens-list",
										"text": "Dispensaciones"
									},
									{
										"id":3,
										"url": "receptions-list",
										"text": "Recepciones"
									}
								]
							}
						]
					}]
				},
				{
					"name": "Admin",
					"access": 
					[{
						"module": "SF",
						"items": [
							{
								"id": 0,
								"icon": "ifa fa fa-th",
								"item": "Farmacia",
								"subitems": [
									{
										"id":0,
										"url": "reception",
										"text": "Recepción"
									},
									{
										"id":1,
										"url": "dispensation",
										"text": "Dispensación"
									}
								]
							},
							{
								"id": 1,
								"icon": "ifa fa fa-user",
								"item": "Programación de Pacientes",
								"subitems": [
									{
										"id":0,
										"url": "patient",
										"text": "Creación"
									},
									{
										"id":1,
										"url": "patient-programing",
										"text": "Programación"
									},
									{
										"id":2,
										"url": "query-schedule",
										"text": "Consultar Agenda"
									}
								]
							},
							{
								"id": 2,
								"icon": "ifa fa fa-list-alt",
								"item": "Listados",
								"subitems": [
									{
										"id":0,
										"url": "patient-list",
										"text": "Pacientes"
									},
									{
										"id":1,
										"url": "product-list",
										"text": "Productos"
									},
									{
										"id":2,
										"url": "dispens-list",
										"text": "Dispensaciones"
									},
									{
										"id":3,
										"url": "receptions-list",
										"text": "Recepciones"
									}
								]
							}
						]
					}]
				}
			]
		},
		"birthdate": "1999-03-12",
		"phone": "3003003030",
		"photo": "resources/profile_photos/yeiapbega_234312",
		"address": {
			"town": "Monteria",
			"adInfo": "En mi casa"
		},
		"email": "ad@gmail.com",
		"accessData": {
			"nickname": "yeiapbega",
			"password": "123123",
			"token": "ashg23hgqh2g1hg123g"
		}
	}
}