const Contact = require('../Models/contactModel')

class table 
{
    store(request) 
    {
        let Data = new Contact(request)        
        var error = Data.validateSync();
        if (error) 
        {
            let status = [];
            for (const key in error.errors) 
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi, "")
                };
                status.push(res)
            }
            return status;
        } else 
        {
            let status = Data.save();
            return status;
        }
    }    
    async Cancel(request) {
        // let Data = new Contact(request) 
        let data = await Contact.findOne({slug: request.slug, nitClient: request.nitClient})
        if(data != null)
        {
            // return {data:data.status, _id: 1}
            data.status=request.status
            let error = data.validateSync()
            if(error)
            {
                var fields = []
                for (const key in error.errors) {
                    let result = {field: error.errors[key].path, feedback: error.errors[key].message.replace(/\\|"/gi,"")}
                    fields.push(result);
                    result = null
                }
                return fields
            }
            else{
                let reg = data.save()
                return reg
            }
        }else
        {
            return 'Failed, document not found'
        }
    }
}
let Contacts = new table();
module.exports = {Contacts};