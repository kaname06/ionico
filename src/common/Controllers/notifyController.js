const Notification = require('../Models/notifyModel')

class table 
{
    store(request) 
    {
        let Data = new Notification(request)        
        var error = Data.validateSync();
        if (error) 
        {
            let status = [];
            for (const key in error.errors) 
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi, "")
                };
                status.push(res)
            }
            return status;
        } else 
        {
            let status = Data.save();
            return status;
        }
    }
    async read(request)
    {
        let data = await Notification.findOne({ _id: request})
        if (data != null) 
        {            
                if (data.state == 'UNREAD') 
                {
                    data.state = 'READ';
                    let error = data.validateSync()
                    if (error) {
                        var fields = []
                        for (const key in error.errors) {
                            let result = {
                                field: error.errors[key].path,
                                feedback: error.errors[key].message.replace(/\\|"/gi, "")
                            }
                            fields.push(result);
                            result = null
                        }
                        return fields
                    } else {                        
                        let res = await data.save()
                        let a = {data: res, status: true}
                        return a;
                    }
                }        
        }
    }
}
let Notify = new table();
module.exports = {Notify};