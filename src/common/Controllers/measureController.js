const Measure = require('../Models/measureModel')

class table 
{
    store(request) 
    {
        let Data = new Measure(request)        
        var error = Data.validateSync();
        if (error) 
        {
            let status = [];
            for (const key in error.errors) 
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi, "")
                };
                status.push(res)
            }
            return status;
        } else 
        {
            let status = Data.save();
            return status;
        }
    }
    async addUm(request) {
        // let dataa = await Measure.findOne({ _id: request.id,company: request.nit })
        let dataa = await Measure.findOne({ _id: request.id})
        if(dataa != null)
        {
            dataa.measurement.push(request.data)
            let error = dataa.validateSync()
            if(error)
            {
                var fields = []
                for (const key in error.errors) {
                    let result = {field: error.errors[key].path, feedback: error.errors[key].message.replace(/\\|"/gi,"")}
                    fields.push(result);
                    result = null
                }
                return fields
            }
            else{
                let reg = dataa.save()
                return reg
            }
        }else
        {
            return 'Failed, document not found'
        }
    }
    async modUm(request) {
        // let dataa = await Measure.findOne({ _id: request.id,company: request.nit })
        let dataa = await Measure.findOne({ _id: request.id})
        if(dataa != null)
        {   
            // if(dataa.measurement[request.pos].company == request.nit)
            // {
                for (let index = 0; index < dataa.measurement.length; index++) {
                    var idum = dataa.measurement[index]._id;
                    if(idum.toString() == request.idpm.toString())
                    {
                        dataa.measurement[index].name = request.name
                        dataa.measurement[index].abbreviature = request.abr
                    }
                    
                }
            // dataa.measurement[request.pos].name = request.name 
            // dataa.measurement[request.pos].abbreviature = request.abr
            // }
            let error = dataa.validateSync()
            if(error)
            {
                var fields = []
                for (const key in error.errors) {
                    let result = {field: error.errors[key].path, feedback: error.errors[key].message.replace(/\\|"/gi,"")}
                    fields.push(result);
                    result = null
                }
                return fields
            }
            else{
                // let reg = await dataa.save()
                let reg = dataa.save()
                return reg //se debe encriptar
            }
        }else
        {
            return 'Failed, document not found'
        }
    }
    async pushCum(request) {
        let dataa = await Measure.findOne({ _id: request.id})
        if(dataa != null)
        {   
            var idpos = request.idpos
            // dataa.measurement[3].convertions.push({company:request.company,convertions:[{product:request.idproduct,equals:{name:request.baseunit,quantity:1},pricelist:request.pricelist}]})
            for (let index = 0; index < dataa.measurement.length; index++) {
                var id =  dataa.measurement[index]._id
                var idd = id.toString()
                var idp = idpos.toString()
                
                if(idd == idp)
                {
                    dataa.measurement[index].convertions.push({company:request.company,convertions:[{product:request.idproduct,equals:{name:request.baseunit,quantity:1},pricelist:request.pricelist}]})
                }
            }
            let error = dataa.validateSync()
            if(error)
            {
                var fields = []
                for (const key in error.errors) {
                    let result = {field: error.errors[key].path, feedback: error.errors[key].message.replace(/\\|"/gi,"")}
                    fields.push(result);
                    result = null
                }
                return fields
            }
            else{
                // let reg = await dataa.save()
                let reg = dataa.save()
                return reg
            }
        }else
        {
            return 'Failed, document not found'
        }
    }
}
let Measures = new table();
module.exports = {Measures};