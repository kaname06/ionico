const mongoose = require('mongoose');
const { Schema } = mongoose;

const Notify = new Schema
({
    module: {
        type: String,
        required: true
    },
    from: {
        rol: {
            type: String,
            default: null
        },
        origin: {
            type: String,
            default: null
        }
    },
    to: {
        rol: [{
            type: String,
            default: null
        }],
        destination: {
            type: String,
            default: null
        }
    },
    information: {
        type: String,
        required: true
    },
    link:{
        type: String,        
    },
    typeN: {
        type: String
    },
    emitDate: {
        type: Date,
        default: Date.now
    },
    state: {
        type: String,
        default: 'UNREAD'
    },
    resources: {
        type: String,
        default: null
    }
})

module.exports = mongoose.model('Notification', Notify);