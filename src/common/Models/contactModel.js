const mongoose = require('mongoose');
const { Schema } = mongoose;

const Contact = new Schema
({
    slug: {
        type: Schema.Types.ObjectId,
        auto: true,
        index: true
    },
    nitClient: {
        type: String,
        required: true
    },
    dni: {
        dni: {
            type: String,
            required: true
        },
        type: {
            type: String,
            required: true
        },
        dv: String
    },
    name: {
        first: String,
        second: String
    },
    lastname: {
        first: String,
        second: String
    },
    status: {
        type: Boolean,
        default: true
    },
    contactType: [String],
    address: [{
        department: String,
        town: String,
        adInfo: String
    }],
    phones: [String],
    email: String,
    conCompany: String,
    payConditions: {
        discount: Number,
        credit: Number,
        payModality: String,
        deadLines: Number,
        pricelist:[String]
    },
    tributaryInf: {
        regimen: String,
        category: String,
        typePerson: String
    },
    worker: {
        jobTitle: String,
        costCenter: String,
        seller: Boolean,
        contract: {
            num: String,
            beginDate: Date,
            endDate: Date,
            salary: Number,
            typeContract: String
        }
    },
    linkContact:[Schema.Types.ObjectId]
})

module.exports = mongoose.model('contacts', Contact);