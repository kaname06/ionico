const mongoose = require('mongoose');
const { Schema } = mongoose;

const Table = new Schema(
{
    slug: {type: Schema.Types.ObjectId, auto: true, index: true},
    description: {type: String},
    consecutive: {type: String},
    barcode: String,
    type: {type: String},
    status: {type: Boolean, default: true},
    category: [Schema.Types.ObjectId],
    typeProduct: {type: String},
    categoryProduct: {type: String},
    company: {type: String, required: true}, //company session
    unit:{type: String},
    stock:{
        min:{type:Number},
        max:{type:Number},
        reorderpoint:{
            type:Number
            // validate: 
            // {
            //     validator: Number.isInteger,
            //     message: '{VALUE} is not an integer value',
            //     default: 0
            // }
        }
    },
    prices: {
        tax: {
            name:{
                type:String,
                //required:true
            },
            val:{
                type: Number, 
                default: 0
            }
        },
        modSale:{
            name: String,
            pricelist:[{name:Schema.Types.ObjectId,val:Number}],
            utility: {
                val:{
                    type: Number, default: 0
                },
                cost:{
                    type: Number, default: 0
                }    
            }
        }   
    },        
    linkProduct: Schema.Types.ObjectId
});

module.exports = mongoose.model('Inventory', Table)