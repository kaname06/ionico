const mongoose = require('mongoose')
const { Schema } = mongoose

const Measures = new Schema({
    measure: {
        type: String,
        required: true
    },
    measurement:[{
        company: {
            type:String
        },
        name:{
            type:String,
            required:true
        },
        abbreviature:String,
        status:String,
        
        convertions:[{
            company: {
                type:String
            },
            convertions:[{
            product: Schema.Types.ObjectId,
            equals:{
                name:{
                    type:String,
                    required:true
                },
                quantity:{
                    type:Number,
                    required:true
                }
            },   
            pricelist:[{
                name:String,
                val: Number
            }]
        }]
    }]    
}],
})

module.exports = mongoose.model('measures', Measures)