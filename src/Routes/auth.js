require('express-router-group')
const express = require('express')
const path = require('path');
const router = express.Router();
const updateJsonFile = require('update-json-file')

require('dotenv').config()
// var cryptorjs = require('cryptorjs');
// var myCryptor = new cryptorjs(process.env.CRYPTOR_PASS);
var { encode, decode } = require('../Modules/Utilities/Plugins/cryptor copy')

//session validator, if this function return true the user will be redirected at local dashboard
var Checker = function (req, res, next) {
    if (req.session.user && req.cookies.user_session_id) {
        res.redirect('/');
    } else {
        res.clearCookie('token');   
        // res.cookie('token', null, {expire: 'Thu, 01 Jan 1970 00:00:01 GMT'})     
        next();
    }    
};

const {authController} = require('../Auth/authController')
const user = require('../Auth/Models/authModel')
const admin = require('../Auth/Models/adminsModel')

router.get('/signup', Checker, (req, res) => {
    res.sendFile(path.join(__dirname, '../../public/signup.html'))
})

router.get('/login', Checker, (req, res) => {
    // res.clearCookie('token');
    res.sendFile(path.join(__dirname, '../../public/login.html'))
})

router.post('/sessdata', (req,res) => {
    if (req.session.user && req.cookies.user_session_id) {
        let temp = req.session.user;
        let data = [];
        for (const key in req.body.fields) {
            let a = req.body.fields[key];
            let ths = temp[a];
            data.push(ths)
        }
        let resp = encode(data)
        res.json({data: resp});
    }
    else {
        res.json('failed')
    }
})

router.get('/id', async (req, res) => {
    let data = await user.find(req.body.id)
    var jsonenc = encode(data);
    var resp = {
        status: "success",
        res: jsonenc
    }
    res.json(resp)
})

router.post('/authinfo', async (req, res) => {
    let data;
    if(req.body.rol == 'user')
        data = await user.find(req.body.validations, req.body.fields)
    if(req.body.rol == 'admin')
        data = await admin.find(req.body.validations, req.body.fields)

    if(!('_id' in data)){
        var resp = {result: data, status: "failed"}
    }else {
        var resp = {result: data, status: "success"}
    }
    res.json(resp)
})

router.post('/info', async (req, res) => {    
    let data;
    if (req.body.rol == 'user')
        data = await user.find()

    var encoded = encode(data);
    var resp = {
        status: "success",
        result: encoded
    }
    res.json(resp)
})

router.post('/store', async (req,res) => {
    let resp = {}
    let data = await authController.store(req.body.data, req.body.type, req.body.license);
    if(!('_id' in data)){
            resp = {result: data, status: "failed"}
    }else {
            resp = {result: data, status: "success"}
    }
    res.json(resp)
})

router.post('/blockStore', async (req, res) => {
    let success = []
    let fails = []
    for (const key in req.body) {
        let data = await authController.store(req.body[key].data, req.body[key].type, req.body[key].license);
        if(!('_id' in data)){
            fails.push({key, data})
        }else {
            success.push(data.name.first)
        }
    }
    res.json({fails, success})
})

router.post('/validate', async (req, res) => {
        let dataAuth = decode(req.body.data) //decode data
       console.log(dataAuth)
       let typeUser = ''
       let authToken = req.body.token
       let vali = false       
       let status = 'error al iniciar sesión'
       if(authToken != '')
       {
            res.cookie('token', `${authToken}`);
       }
       if(dataAuth && ('_id' in dataAuth))
       {            
            if(dataAuth.profile.access == 'All Granted' && dataAuth.company.rols)
            {
                let r = dataAuth.company.rols[0].rols                
                let roll = null
                for (const key in r) 
                {                    
                    if(r[key].name == 'Admin') 
                    {                        
                        roll = r[key].items
                    }
                }                
                let role = {name: dataAuth.rol, access: roll}
                let compish = {name: dataAuth.company.name, nit: dataAuth.company.nit, logo: dataAuth.company.logo}
                let datitoss = {
                    name: dataAuth.name,
                    dni: dataAuth.dni,
                    _id: dataAuth._id,
                    rol: role,
                    email: dataAuth.email,
                    address: dataAuth.address,
                    photo: dataAuth.photo,
                    phone: dataAuth.phone,
                    company: compish,
                    birthdate: dataAuth.birthdate,
                    lastAccess: dataAuth.lastAccess
                }                    
                req.session.user = datitoss

                if(req.session && req.session.user)
                {
                    vali = true
                    status = 'Authenticated!'
                }
            }
            else
            {
                console.log(dataAuth.profile.access);
                let datosss = {
                    name: dataAuth.name,
                    dni: dataAuth.dni,
                    _id: dataAuth._id,
                    rol: dataAuth.profile,
                    email: dataAuth.email,
                    address: dataAuth.address,
                    photo: dataAuth.photo,
                    phone: dataAuth.phone,
                    company: dataAuth.company,                    
                    lastAccess: dataAuth.lastAccess
                }
                req.session.user = datosss
               
                if(req.session && req.session.user)
                {
                    vali = true
                    status = 'Authenticated!'
                } 
            }
       }
       else
       {
        vali = false
       }
       let la = new Date()
       la.setHours(la.getHours() - 5)

       let lau = '' //here query update last access 
    //    console.log(req!.session!.user)
        return res.json({validation:vali,info:status})
    })

    // let dat = decode(req.body.data);
    // let nick = dat.nickname
    // let pass = dat.password
    // let a = nick.split('_')
    // let resp = ''
    // let rol = 'Usuario'
    // if(a.length == 2) {
    //     if(a[0] == "admin") {
    //         rol = 'Administrador'
    //         const data = await admin.findOne({'accessData.nickname': a[1]})
    //         if(data) {
    //             if(data && data.validatePass(pass)){
    //                 resp = 'Authenticated'
    //                 let roll = '';
    //                 for (const key in data.company.rols) 
    //                 {
    //                     if(data.company.rols[key].name == 'Admin') 
    //                     {
    //                         roll = data.company.rols[key].access[0]
    //                     }
    //                 }
    //                 let role = {name: data.rol, access: roll}
    //                 let compish = {name: data.company.name, nit: data.company.nit, logo: data.company.logo}
    //                 let datitoss = {
    //                     name: data.name,
    //                     dni: data.dni,
    //                     _id: data._id,
    //                     rol: role,
    //                     email: data.email,
    //                     address: data.address,
    //                     photo: data.photo,
    //                     phone: data.phone,
    //                     company: compish,
    //                     birthdate: data.birthdate,
    //                     lastAccess: data.accessData.lastAccess
    //                 }                    
    //                 req.session.user = datitoss
    //                 updateJsonFile('u23r20nl1n3.txt', (dat) => {
    //                     dat.push({dni: req.session.user.dni.dni})
    //                     return dat;
    //                 })
    //                 let lafec = new Date()
    //                 lafec.setHours(lafec.getHours() - 5)
    //                 data.accessData.lastAccess = lafec;
    //                 await data.save();
    //             }
    //             else
    //                 resp = 'Contraseña incorrecta'
    //         }
    //         else
    //             resp = 'Admin no encontrado'
    //     }
    //     else
    //         resp = 'Datos invalidos'
    // }
    // else {
    //     let dataa = await user.findOne({'accessData.nickname': nick})        
    //     if(dataa) {
    //         if(dataa && dataa.validatePass(pass)) {
    //             resp = 'Authenticated'                
    //             let comp = dataa.company
    //             let compname = {};
    //             let rol = dataa.rol
    //             let bck = {};
    //             let query = await admin.find()
    //             for (const key in query) {
    //                 if(query[key].company._id == comp.toString()) {
    //                     let rols = query[key].company.rols
    //                     compname.name = query[key].company.name;
    //                     compname.nit = query[key].company.nit;
    //                     compname.logo = query[key].company.logo
    //                     for (const llave in rols) {
    //                         if(rols[llave]._id == rol.toString()) {
    //                             bck.name = rols[llave].name;
    //                             bck.access = rols[llave].access[0]
    //                         }
    //                     }
    //                 }
    //             }
    //             dataa.accessData.lastAccess = new Date();
    //             await dataa.save()

                // let datosss = {
                //     name: dataa.name,
                //     dni: dataa.dni,
                //     _id: dataa._id,
                //     rol: bck,
                //     email: dataa.email,
                //     address: dataa.address,
                //     photo: dataa.photo,
                //     phone: dataa.phone,
                //     company: compname,
                //     birthdate: dataa.birthdate,
                //     lastAccess: dataa.accessData.lastAccess
                // }
                // req.session.user = datosss
    //             updateJsonFile('u23r20nl1n3.txt', (dat) => {
    //                 dat.push({dni: req.session.user.dni.dni})
    //                 return dat;
    //             })
    //         }
    //         else
    //             resp = 'Constraseña incorrecta'
    //     }
    //     else
    //         resp = 'Usuario no encotrado'
    // }
    // var status = 'success'
    // if(resp != 'Authenticated')
    //     status = 'failed'
    // res.json({rol: rol, response: resp, status: status})


module.exports = router