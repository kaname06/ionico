//*------ Plugins Initialize
require('express-router-group')
const express = require('express')
const router = express.Router();

require('dotenv').config()
var cryptorjs = require('cryptorjs');
var myCryptor = new cryptorjs(process.env.CRYPTOR_PASS);

//!------ CONTROLLERS
//! Reception, Product, Patient
let {receptionController} = require('../Modules/SF/Controllers/receptionController');
// let {productController} = require('../Modules/SF/Controllers/productController');
let {ContactsController} = require('../Modules/SF/Controllers/dynamicTableController');
let {Notify} = require('../common/Controllers/notifyController');
let {patientController} = require('../Modules/SF/Controllers/patientController');
let {scheduleController} = require('../Modules/SF/Controllers/scheduleController');
let {dispenController} = require('../Modules/SF/Controllers/dispensationController');
let {productController} = require('../common/Controllers/inventoryController');
let {prodController} = require('../Modules/SF/Controllers/productController');
const {Measures} = require('../common/Controllers/measureController')

//TODO------ ROUTES
//TODO: NOTIFICATIONS
router.group('/notification', router => 
{
    router.post('/store', async (req, res) =>
    {
        let data = await Notify.store(req.body)
        if (!('_id' in data)) {
            var resp = {
                result: data,
                status: "failed"
            }
        } else {
            var resp = {
                result: data,
                status: "success"
            }            
        }
        res.json(resp)
    })
})
// //TODO: PRODUCT
router.group('/product', router =>
{
    router.post('/store', async (req, res) =>
    {
        let data = await prodController.store(req.body)
        if (!('_id' in data)) {
            var resp = {
                result: data,
                status: "failed"
            }
        } else {
            let data_ = await productController.store({linkProduct: data._id, company: req.body.companySession})
            if('_id' in data_)
            {
                var resp = {
                    result: data,
                    status: "success"
                }
            }
            else
            {
                var resp = {
                    result: data_,
                    status: "failed inventory"
                }
            }
        }
        res.json(resp)
    })
})

//TODO: SCHEDULE
router.group('/schedule', router =>
{
    router.post('/store', async (req, res) =>
    {
        let data = await scheduleController.store(req.body)
        if (!('_id' in data)) {
            var resp = {
                result: data,
                status: "failed"
            }
        } else {
            var resp = {
                result: data,
                status: "success"
            }
            req.session.tempProgInfo = data
        }
        res.json(resp)
    })

    router.post('/up', async (req, res) =>
    {
        let data = await scheduleController.update(req.body)
        if (!('_id' in data)) {
            var resp = {
                result: data,
                status: "failed"
            }
        } else {
            var resp = {
                result: data,
                status: "success"
            }
        }
        res.json(resp)
    })

    router.post('/deldate', async (req, res) => {
        let id = req.body.id
        let target = req.body.date
        let data = await scheduleController.deleteItem(id, target)
        if (!('_id' in data)) {
            var resp = {
                result: data,
                status: "failed"
            }
        } else {
            var resp = {
                result: data,
                status: "success"
            }
        }
        res.json(resp)
    })
})

//TODO: RECEPTION
router.group('/reception', router => 
{
    router.post('/store', async (req, res) => 
    {
        let data = await receptionController.store(req.body.data)
        //    var encoded = myCryptor.encode(a);
        if (!('_id' in data)) {
            var resp = {
                result: data,
                status: "failed"
            }
        } else {

            var resp = {
                result: data,
                status: "success"
            }
        }
        res.json(resp)
    });

    router.put('/up', async (req, res) => 
    {
        let data = await receptionController.approvedProducts(req.body.id, req.body.data, req.body.who)
        var encoded = myCryptor.encode(data);
        if(data.status == 'success')
        {
            res.json({status: 'success', data: encoded})
        } else {
            res.json({status: 'error'})
        }
    })
})

//TODO: CONTACT
router.group('/contact', router => 
{
    router.post('/store', async (req, res) => 
    {
        let data = await ContactsController.store(req.body.data)
        var encoded = myCryptor.encode(data);
        if (!('_id' in data)) {
            var resp = {
                result: encoded,
                status: "failed"
            }
        } else {
            var resp = {
                result: encoded,
                status: "success"
            }
        }
        res.json(resp)
    });
    router.put('/update', async (req, res) => 
    {
        let data = await ContactsController.update(req.body.id, req.body.datas)  
        console.log(data)      
        if(data.status == 'success')
        {
            var encoded = myCryptor.encode(data.data)
            res.json({status: true, data: encoded})
        } else{
            res.json({status: false})
        }
    })
    router.post('/del', async (req, res) => 
    {
        let data = await ContactsController.del(req.body.id)  
        console.log(data)      
        if(data.status == 'success')
        {
            var encoded = myCryptor.encode(data.data)
            res.json({status: true, data: encoded})
        } else{
            res.json({status: false})
        }
    })
})

//TODO: INVENTORY
router.group('/inventory', router => 
{
    router.post('/store', async (req, res) => 
    {
        let data = await productController.store(req.body)
        if (!('_id' in data)) {
            var resp = {
                result: data,
                status: "failed"
            }
        } else {
            var resp = {
                result: data,
                status: "success"
            }
        }
        res.json(resp)
    });
})

//TODO: PATIENT
router.group('/patient', router =>
{
    router.post('/store', async (req, res) =>
    {
        let data = await patientController.store(req.body)
        if (!('_id' in data)) {
            var resp = {
                result: data,
                status: "failed"
            }
        } else {
            var resp = {
                result: data,
                status: "success"
            }
        }
        res.json(resp)
    })        
})

//TODO: DISPENSATION
router.group('/dispensation', router => {
    router.post('/store', async (req, res) =>
    {
        let data = await dispenController.store(req.body)
        if (!('_id' in data)) {
            var resp = {
                result: data,
                status: "failed"
            }
        } else {
            var resp = {
                result: data,
                status: "success"
            }
        }
        res.json(resp)
    }) 
    
    router.post('/verifyR', async (req, res) => {
        let data = await dispenController.verifyRecep(req.body.id)
        res.json(data)
    })
})

//TODO: MEASURE
//-----Measures Group
router.group('/measures', (router) => {
    router.post('/store', async (req, res) => {
        let data = await Measures.store(req.body);
        if (!('_id' in data)) {
            var resp = {
                result: data,
                status: "failed"
            }
        } else {
            var resp = {
                result: data,
                status: "success"
            }
        }
        res.json(resp)
    })
    router.put('/measurement', async (req, res) => {
        let data = await Measures.addUm(req.body);
        if (!('_id' in data)) {
            var resp = {
                result: data,
                status: "failed"
            }
        } else {
            var resp = {
                result: data,
                status: "success"
            }
        }
        res.json(resp)
    })
})

module.exports = router;