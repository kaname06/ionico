//Dependencies
require('express-router-group')
const express = require('express')
const excel = require('node-excel-export')
const router = express.Router();

//Plugins initilize
var cryptorjs = require('cryptorjs');
require('dotenv').config()
var myCryptor = new cryptorjs(process.env.CRYPTOR_PASS);
var address = require('address');

router.post('/ip', (req, res) =>
{
    // let ip = address.ip();
    let ip = req.connection.remoteAddress
    res.json(myCryptor.encode(ip))
})
//TODO: COMMON MODELS
//-----Contact
const Contacts = require('../common/Models/contactModel')

//-----Notification
const NotifyModel = require('../common/Models/notifyModel')
const { Notify } = require('../common/Controllers/notifyController')

//! SC
//Models
//-----Concepts
const ConceptModel = require('../Modules/SC/Models/conceptsModel')

//-----Inventory
const invModel = require('../common/Models/inventoryModel')
//-----PriceList
const plModel = require('../Modules/SC/Models/pricelistModel')
//-----Department
const DepartModel = require('../Modules/SC/Models/countriesModel')
//-----Contact
const ContactModel = require('../common/Models/contactModel')
//-----Auditory Logs
const LogsModel = require('../Modules/SC/Models/logsModel')
//-----Categories
const catModel = require('../Modules/SC/Models/categoriesModel')
//-----Measures
const MeasureModel = require('../common/Models/measureModel')
//-----Cellars
const CellarModel = require('../Modules/SC/Models/cellarsModel')
//Routes Groups

router.group('/com', (router) => {
    router.group('/contacts', (router) => {
        router.post('/list', async (req, res) => {
            let data = await Contacts.find()
            var jsonenc = myCryptor.encode(data);
            var resp = {
                status: "success",
                res: jsonenc
            }
            res.json(resp)
        })
        // router.post('/del', async (req, res) => {
        //     console.log(req.body.id)
        //     let data = await Contacts.findOneAndDelete({_id: req.body.id})
        //     console.log(data)
        //     var encoded = myCryptor.encode(data);
        //     if('_id' in data)
        //     {
        //         res.json({status: 'success', data: encoded})
        //     } else {
        //         res.json({status: 'error'})
        //     }
        // })
    })
    router.group('/notification', (router) => {
        router.post('/list', async (req, res) => {
            let data = await NotifyModel.find(req.body.query).sort({emitDate: -1 })
            var jsonenc = myCryptor.encode(data);
            var resp = {
                status: "success",
                res: jsonenc
            }
            res.json(resp)
        })
        router.post('/store', async (req, res) => {
            let data = await Notify.store(req.body);
            var jsonenc = myCryptor.encode(data);
            if(!('_id' in data)){                
                var resp = {result: jsonenc, status: "failed"}
            }else {
                var resp = {result: jsonenc, status: "success"}
            }
            res.json(resp)
        })
        router.put('/read', async (req, res) => 
        {
            let data = await Notify.read(req.body.id);   
            var jsonenc = myCryptor.encode(data);
            if(data.status)
            {
                var resp = {result: jsonenc, status: "success"}
            } else{
                var resp = {result: jsonenc, status: "failed"}
            }
            res.json(resp)
        })
    })
    router.group('/cookie', (router) => {
        router.post('/room', (req, res) => {
            // galleta.set('tusnalgas', req.body.data)
            // console.log(galleta)
            res.cookie('tusnalgas','algo we').json('algo')
        })
    });
})


router.group('/sc', (router) => {
    //-----Departments Group
    router.group('/depart', (router) => {
        router.post('/list', async (req, res) => {
            let data = await DepartModel.find()
            var jsonenc = myCryptor.encode(data);
            var resp = {
                status: "success",
                res: jsonenc
            }
            res.json(resp)
        })
    })

    //-----Concepts Group
    router.group('/concepts', (router) => {    
        router.post('/list', async (req, res) => {
            let data = await ConceptModel.find()
            var jsonenc = myCryptor.encode(data);
            var resp = {
                status: "success",
                res: jsonenc
            }
            res.json(resp)
        })
    })

    //-----Contacts Group
    router.group('/contact', (router) => {
        router.post('/list', async (req, res) => {
            let data = await ContactModel.find({nitClient: req.body.dni,status:req.body.status})
            var jsonenc = myCryptor.encode(data);
            var resp = {
                status: "success",
                res: jsonenc
            }
            res.json(resp)
        })
        router.post('/listone', async (req, res) => {
            let data = await ContactModel.findOne({slug:req.body.slug},{_id:0})
            var jsonenc = myCryptor.encode(data);
            var resp = {
                status: "success",
                res: jsonenc
            }
            res.json(resp)
        })
        router.post('/val-dni', async (req, res) => {
            let data = await ContactModel.findOne({'dni.dni': req.body.dni});
            if (data != null) {
                res.json('fail')
            } else {
               res.json('ok')
            }
        })
        router.post('/listworker', async (req, res) => {
            let data = await ContactModel.find({nitClient: req.body.dni,status:req.body.status,contactType:req.body.contype})
            var jsonenc = myCryptor.encode(data);
            var resp = {
                status: "success",
                res: jsonenc
            }
            res.json(resp)
        })
    })

    //-----Logs Group
    router.group('/logs', (router) => {
        router.post('/list', async (req, res) => {
            let data = await LogsModel.find()
            var jsonenc = myCryptor.encode(data);
            var resp = {
                status: "success",
                res: jsonenc
            }
            res.json(resp)
        })
    })

    //------Inventory Group
    router.group('/inventory', (router) => {
        router.post('/list', async (req, res) => {
            let data = await invModel.find({company:req.body.nit,status:true})
            var jsonenc = myCryptor.encode(data);
            res.json(jsonenc)
        })
        router.post('/val-code', async (req, res) => {
            let data = await invModel.findOne({'company':req.body.nit,'consecutive': req.body.code});
            // var jsonenc = myCryptor.encode(data);
            if (data != null) {
                res.json('fail')
            } else {
               res.json('ok')
            }
        })
        router.post('/val-barcode', async (req, res) => {
            let data = await invModel.findOne({'company':req.body.nit,'barcode': req.body.barcode});
            // var jsonenc = myCryptor.encode(data);
            if (data != null) {
                res.json('fail')
            } else {
               res.json('ok')
            }
        })
    })

    //------Category Group
    router.group('/categories', (router) => {
        router.post('/list', async (req, res) => {
            let data = await catModel.find({company:req.body.nit})
            var jsonenc = myCryptor.encode(data);
            res.json(jsonenc)
        })
    })
    //------PriceList Group
    router.group('/pricelist', (router) => {
        router.post('/list', async (req, res) => {
            let data = await plModel.find({company:req.body.nit,status:true})
            var jsonenc = myCryptor.encode(data);
            res.json(jsonenc)
        })
    })
    //------Measures Group
    router.group('/measures', (router) => {
        router.post('/list', async (req, res) => {
            let data = await MeasureModel.find()
            let array_data = []
            let measurement= []
            let nit = req.body.nit
            for (let index = 0; index < data.length; index++) {
                var id = data[index]._id
                var um = data[index].measure
                var pum = data[index].measurement
                for (let indexe = 0; indexe < pum.length; indexe++) {
                    var pum_id = pum[indexe]._id
                    var pum_name = pum[indexe].name
                    var pum_abbrev = pum[indexe].abbreviature
                    var pum_status = pum[indexe].status
                    var pum_company = pum[indexe].company
                    var pum_id = pum[indexe]._id
                    if((pum_status == "default" || pum_company == nit) && pum_status != 'delete') {
                        // measurement.push({name:pum_name,abbreviature:pum_abbrev,status:pum_status,idpos:pum_id})
                        measurement.push({id:pum_id, name:pum_name,abbreviature:pum_abbrev,status:pum_status,pos:indexe})
                    } 
                }
                array_data.push({_id:id,measure:um,measurement})
                measurement = []
            }
            var jsonenc = myCryptor.encode(array_data);
            res.json(jsonenc)
        })
    })
      //------Cellars Group
      router.group('/cellar', (router) => {
        router.post('/list', async (req, res) => {
            let data = await CellarModel.find({company:req.body.nit,status:true})
            let data_cellar = []
            for (let index = 0; index < data.length; index++) {
                var cename = data[index].name;
                var cedescription = data[index].description;
                var id = data[index]._id
                var pos = index
                var loc = data[index].location
                data_cellar.push({_id:id,name:cename,description:cedescription,location:loc,position:pos})  
            }
            var jsonenc = myCryptor.encode(data_cellar);
            res.json(jsonenc)
        })
    })
})

//! SF

//?---- MODELS
//? Reception, Product, DynamicTable, Patient, Schedule
const ReceptionModel = require('../Modules/SF/Models/receptionModel');
const ProductModel = require('../Modules/SF/Models/productModel');
const DynamicTableModel = require('../Modules/SF/Models/dynamicTableModel')
const PatientModel = require('../Modules/SF/Models/patientModel');
const ScheduleModel = require('../Modules/SF/Models/scheduleModel');
const dispensModel = require('../Modules/SF/Models/dispensationModel');

//TODO------ ROUTES
router.group('/sf', (router) => {
    //TODO: PRODUCT
    router.group('/product', router => {
        router.post('/list', async (req, res) => {
            let data = await ProductModel.find()
            var encoded = myCryptor.encode(data);
            res.json(encoded);
        })

        router.post('/verifyCnc', async (req, res) => {
            let cnc = await ProductModel.findOne({
                consecutive: req.body.cnc
            });
            if (cnc) {res.json(true)}
            else {res.json(false)}
        })

        router.post('/del', async (req, res) => {
            let data = await ProductModel.findOneAndDelete({ _id: req.body.id })
            var encoded = myCryptor.encode(data);
            if('_id' in data)
            {
                res.json({status: 'success', data: encoded})
            } else {
                res.json({status: 'error'})
            }
        })

        router.put('/up', async (req, res) => {
            let data = await ProductModel.findByIdAndUpdate(req.body.id, req.body.data)
            var encoded = myCryptor.encode(data);
            if('_id' in data)
            {
                res.json({status: 'success', data: encoded})
            } else {
                res.json({status: 'error'})
            }
        })

        router.post('/unique-code', async (req, res) => 
        {
            console.log(req.body.code)
            let data = await ProductModel.findOne({
                'code_cum': req.body.code
            });
            if (data != null) {
                res.json('found')
            } else {
                res.json('pass')
            }
        })
    })

    //TODO: RECEPTION
    router.group('/reception', router => {
        router.post('/list', async (req, res) => {
            let data = await ReceptionModel.find({$or:[ {'status':'WAIT'}, {'status':'BACK'}, {'status':'BACK_'}, {'status':'SUCCESS'}]}).sort({date_creation: -1 })
            var encoded = myCryptor.encode(data);
            res.json(encoded)
        })
        router.post('/changestat', async (req, res) => {
            let data = await ReceptionModel.findByIdAndUpdate(req.body.id, req.body.data)
            var encoded = myCryptor.encode(data);
            if ('_id' in data) {
                res.json({
                    status: 'success',
                    data: encoded
                })
            } else {
                res.json({
                    status: 'error'
                })
            }
        })
    })

    //TODO: DYNAMIC TABLE
    router.group('/dynamictable', router => {
        router.post('/list', async (req, res) => {
            let data = await DynamicTableModel.find()
            var encoded = myCryptor.encode(data);
            var resp = {
                status: "success",
                res: encoded
            }
            res.json(resp)
        })
        router.post('/unique-nit', async (req, res) => 
        {
            console.log(req.body.nit)
            let data = await DynamicTableModel.findOne({
                'nitClient': req.body.nit
            });            
            if (data != null) 
            {
                res.json('found')
            } else {
                res.json('pass')
            }
        })
    })

    //TODO: PATIENT
    router.group('/patient', router => {
        router.post('/list', async (req, res) => {
            let data = await PatientModel.find({'basic_info.live': true})
            var encoded = myCryptor.encode(data);
            res.json(encoded)
        })

        router.post('/unique-dni', async (req, res) => {
            let data = await PatientModel.findOne({
                'dni.num': req.body.dni
            });
            if (data != null) {
                res.json('found')
            } else {
                res.json('pass')
            }
        //  var encoded = myCryptor.encode(data);
        //  res.json(encoded)
        })
        router.post('/del', async (req, res) => 
        {
            let data = await PatientModel.findByIdAndUpdate(req.body.id, {'basic_info.live': false})
            if('_id' in data)
            {
                var encoded = myCryptor.encode(data);
                res.json({status: 'success', data: encoded})
            } else {
                res.json({status: 'error'})
            }
        })
        router.put('/up', async (req, res) => 
        {
            let data = await PatientModel.findByIdAndUpdate(req.body.id, req.body.datas)
            if('_id' in data)
            {
                var encoded = myCryptor.encode(data);
                res.json({status: 'success', data: encoded})
            } else {
                res.json({status: 'error'})
            }
        })
    })
    //TODO: SCHEDULE
    router.group('/schedule', router => {
        router.post('/list', async (req, res) => {
            let data = await ScheduleModel.find()
            // console.log("data:"+data)
            var encoded = myCryptor.encode(data);
            res.json(encoded)
        })

        router.post('/getTempProg', (req, res) => {
            let data = req.session.tempProgInfo
            if(req.session.tempProgInfo) {
                res.json({status: true, data})
            }
            else
                res.json({status: false})
        })
    })
    //TODO: DISPENSATION
    router.group('/dispensation', router => {
        router.post('/list', async (req, res) => {
            let data = await dispensModel.find()
            var encoded = myCryptor.encode(data);
            res.json(encoded)
        })
    })
})

//COMMON
const Product = require('../common/Models/inventoryModel');

router.group('/common', (router) => {
    router.group('/inv', (router) => {
        router.post('/list', async (req, res) => {
            let data = await Product.find()
            var encoded = myCryptor.encode(data);
            res.json(encoded)
        })
    })

    router.group('/excel', (router) => {
        router.post('/tempData', (req, res) => {
            req.session.tempColl = req.body
            res.json('Saved')
        })
        router.get('/SFschedule', async (req, res) => {
            let data = req.session.tempColl.data
                let maxl = 0;
                for (const key in data) {
                    if(data[key].drugs.length > maxl) {
                        maxl = data[key].drugs.length
                    }
                }
                
                const styles = {
                    headerFills: {
                        font: {
                            color: {
                                rgb: '000000'
                            },
                            sz: 14,
                            bold: true,
                            underline: true
                        },
                        alignment: {
                            horizontal: 'center',
                            vertical: 'center'
                        }
                    },
                    headerNormal: {
                        font: {
                            color: {
                                rgb: '000000'
                            },
                            sz: 14,
                            bold: true,
                            underline: true
                        }
                    },
                    headerContent: {
                        font: {
                            color: {
                                rgb: '000000'
                            },
                            sz: 17,
                            bold:true
                        },
                        alignment: {
                            horizontal: 'center'
                        }
                    },
                    cell: {
                        alignment: {
                            horizontal: 'center'
                        }
                    }
                }
                let lafe = ''
                if(data.range != null)
                    lafe = data.range[0].getDate() +'/'+(data.range[0].getMonth()+1)+'/'+data.range[0].getFullYear()+' - '+data.range[1].getDate()+'/'+(data.range[1].getMonth()+1)+'/'+data.range[0].getFullYear()
                else
                    lafe = 'Total de Programaciones Registradas'
                const heading = [
                    [{value: 'IONICO', style: styles.headerContent}],
                    [{value: 'REPORTE DE PROGRAMACIONES', style: styles.headerContent}],
                    [{value: lafe, style: styles.headerContent}],
                    [,,,,,,,,,{value: 'Medicamentos', style: styles.headerFills}]
                ];

                const specification = {
                    dni: {
                        displayName: 'Identificación',
                        headerStyle: styles.headerFills,
                        cellStyle: styles.cell,
                        width: 200,
                        heigth: 50
                    },
                    patient: {
                        displayName: 'Paciente',
                        headerStyle: styles.headerFills,
                        cellStyle: styles.cell,
                        width: 250
                    },
                    module: {
                        displayName: 'Silla/Cama',
                        headerStyle: styles.headerFills,
                        cellStyle: styles.cell,
                        width: 220,
                        cellFormat: function(value,_) {
                            return (value) ? value : ''
                        }
                    },
                    meeting: {
                        displayName: 'Fecha Cita',
                        headerStyle: styles.headerFills,
                        cellStyle: styles.cell,
                        width: 150
                    },
                    hour: {
                        displayName: 'Hora Cita',
                        headerStyle: styles.headerFills,
                        cellStyle: styles.cell,
                        width: 100
                    },
                    schemaN: {
                        displayName: 'Esquema',
                        headerStyle: styles.headerFills,
                        cellStyle: styles.cell,
                        width: 220
                    },
                    transport: {
                        displayName: 'Vehículo',
                        headerStyle: styles.headerFills,
                        cellStyle: styles.cell,
                        width: 250,
                        cellFormat: function(value,_) {
                            return (value) ? value : ''
                        }
                    },
                    pckUnit: {
                        displayName: 'Unidad de empaque',
                        headerStyle: styles.headerFills,
                        cellStyle: styles.cell,
                        width: 250,
                        cellFormat: function(value,_) {
                            return (value) ? value : ''
                        }
                    },
                    volume: {
                        displayName: 'Volumen de mezcla',
                        headerStyle: styles.headerFills,
                        cellStyle: styles.cell,
                        width: 250,
                        cellFormat: function(value,_) {
                            return (value) ? value : ''
                        }
                    }
                }

                for (let index = 0; index < maxl; index++) {
                    specification[`med-${index + 1}`] = {
                        displayName: `Medicamento ${index + 1}`,
                        headerStyle: styles.headerFills,
                        cellStyle: styles.cell,
                        width: 300,
                        cellFormat: function(value,_) {
                            return (value) ? value : ''
                        }
                    }

                    specification[`cnc-${index + 1}`] = {
                        displayName: `Mg`,
                        headerStyle: styles.headerFills,
                        cellStyle: styles.cell,
                        width: 75,
                        cellFormat: function(value,_) {
                            return (value) ? value : ''
                        }
                    }

                    specification[`pres-${index + 1}`] = {
                        displayName: `Presentación`,
                        headerStyle: styles.headerFills,
                        cellStyle: styles.cell,
                        width: 200,
                        cellFormat: function(value,_) {
                            return (value) ? value : ''
                        }
                    }
                }

                const dataset = data;
                for (let key = 0; key < dataset.length; key++) {
                    let meds = dataset[key].drugs
                    let sch = dataset[key]
                    for (let llave=0; llave < meds.length; llave++) {
                        sch[`med-${llave + 1}`] = meds[llave].name
                        sch[`cnc-${llave + 1}`] = meds[llave].concentration
                        sch[`pres-${llave + 1}`] = meds[llave].present
                    }
                }
                
                const merges = [
                    {start: { row: 1, column: 1}, end: { row: 1, column: (9 + (maxl*3)) }},
                    {start: { row: 2, column: 1}, end: { row: 2, column: (9 + (maxl*3)) }},
                    {start: { row: 3, column: 1}, end: { row: 3, column: (9 + (maxl*3)) }},
                    {start: { row: 4, column: 1}, end: { row: 4, column: 9 }},
                    {start: { row: 4, column: 10}, end: { row: 4, column: (10 + (maxl*3) - 1) }},
                ];
                
                let dat = new Date()
                dat.setMonth(dat.getMonth() + 1)
                let m = ''
                if(dat.getMonth() < 10)
                    m = '0'+dat.getMonth()
                else
                    m = dat.getMonth()
                let parts = dat.getDate()+'-'+m+'-'+dat.getFullYear()
                let nam = 'Programación ('+parts+')'

                const report = excel.buildExport([
                    {
                        name: nam,
                        heading: heading,
                        merges: merges,
                        specification: specification,
                        data: dataset
                    }
                ]);
                res.attachment(nam+'.xlsx');
                return res.send(report);
        })
        router.post('/tempDataPq', (req, res) => {
            // req.session.tempCollp = req.body
            req.session.tempCollp = req.body.datash
            req.session.tempCollpr = req.body.rangedate
            res.json({st:'Saved',data:req.session.tempCollp})
        })
        router.get('/SFprogqui', async (req, res) => {
            let data = req.session.tempCollp
            let ranged = req.session.tempCollpr
            const styles = {
                headerFills: {
                    fill: {
                        fgColor: {
                          rgb: 'E6E0DF'
                        }
                      },
                    font: {
                        color: {
                            rgb: '000000'
                        },
                        sz: 14,
                        bold: true,
                        underline: true
                    },
                    alignment: {
                        horizontal: 'center',
                        vertical: 'center',
                        wrapText: true
                    }
                },
                headerNormal: {
                    font: {
                        color: {
                            rgb: '000000'
                        },
                        sz: 14,
                        bold: true,
                        underline: true
                    }
                },
                headerContent: {
                    fill: {
                        fgColor: {
                          rgb: 'ffffff'
                        }
                      },
                    font: {
                        color: {
                            rgb: '000000'
                        },
                        sz: 17,
                        bold:true
                    },
                    alignment: {
                        horizontal: 'center'
                    }
                },
                cell: {
                    alignment: {
                        horizontal: 'center'
                    }
                }
            }
            const heading = [
                [{value: 'CENTRO DE CANCEROLOGÍA DE BOYACÁ', style: styles.headerContent}],
                [{value: 'FORMATO PROGRAMACIÓN DIARIA EN LA SALA QUIMIOTERAPIA', style: styles.headerContent}],
                [{value: ranged, style: styles.headerContent}],
                [{value: ' ',style: styles.headerContent}]
                // [,,,,,,,,,{value: 'Pacientes', style: styles.headerFills}]
            ];
            const specification = {
                hourin: {
                    displayName: 'Hora De Ingreso',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 150,
                    heigth: 50,
                    cellFormat: function(value,_) {
                        return (value) ? value : ''
                    }
                },
                hourout: {
                    displayName: 'Hora De Salida',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 150,
                    cellFormat: function(value,_) {
                        return (value) ? value : ''
                    }
                },
                datereq: {
                    displayName: 'Fecha De Solicitud',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 200
                },
                meeting: {
                    displayName: 'Fecha De Aplicación',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 200
                },
                module: {
                    displayName: 'Módulo',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 150
                },
                hour: {
                    displayName: 'Hora',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 150
                },
                inpre: {
                    displayName: 'Incidente/Prevalente (I/P)',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 100,
                    height:100
                },
                eps: {
                    displayName: 'EPS',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 150
                },
                firstname: {
                    displayName: 'Primer Nombre',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 150
                },
                secondname: {
                    displayName: 'Segundo Nombre',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 150
                },
                lastfname: {
                    displayName: 'Primer Apellido',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 150
                },
                lastsname: {
                    displayName: 'Segundo Apellido',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 150
                },
                typedni: {
                    displayName: 'Tipo ID',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 150
                },
                dni: {
                    displayName: 'Número de Identificación del usuario',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 150,
                    heigth:100
                },
                gender: {
                    displayName: 'Sexo',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 100
                },
                town: {
                    displayName: 'Municipio',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 150
                },
                phone: {
                    displayName: 'Teléfono',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 150,
                    heigth:100
                },
                cie10: {
                    displayName: 'CIE10',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 150
                },
                drugSchema: {
                    displayName: 'Medicamento o Esquema',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 150
                },
                numAuth: {
                    displayName: 'Número De Autorización',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 150,
                    heigth: 100
                },
                sign: {
                    displayName: 'Firma Del Paciente',
                    headerStyle: styles.headerFills,
                    cellStyle: styles.cell,
                    width: 150,
                    cellFormat: function(value,_) {
                        return (value) ? value : ''
                    }
                }
                  
            }
            const dataset = data;
            const merges = [
                {start: { row: 1, column: 1}, end: { row: 1, column: 21 }},
                {start: { row: 2, column: 1}, end: { row: 2, column: 21 }},
                {start: { row: 3, column: 1}, end: { row: 3, column: 21 }},
                {start: { row: 4, column: 1}, end: { row: 4, column: 21 }},
                {start: { row: 5, column: 1}, end: { row: 5, column: 1 }},
                {start: { row: 5, column: 21}, end: { row: 5, column: 21 }},
            ];
            // const merges = [
            //     {start: { row: 1, column: 1}, end: { row: 1, column: (9 + (maxl*3)) }},
            //     {start: { row: 2, column: 1}, end: { row: 2, column: (9 + (maxl*3)) }},
            //     {start: { row: 3, column: 1}, end: { row: 3, column: (9 + (maxl*3)) }},
            //     {start: { row: 4, column: 1}, end: { row: 4, column: 9 }},
            //     {start: { row: 4, column: 10}, end: { row: 4, column: (10 + (maxl*3) - 1) }},
            // ];
            
            let dat = new Date()
            dat.setMonth(dat.getMonth() + 1)
            let m = ''
            if(dat.getMonth() < 10)
                m = '0'+dat.getMonth()
            else
                m = dat.getMonth()
            let parts = dat.getDate()+'-'+m+'-'+dat.getFullYear()
            let nam = 'Programación Quimio ('+parts+')'

            const report = excel.buildExport([
                {
                    name: nam,
                    heading: heading,
                    merges: merges,
                    specification: specification,
                    data: dataset
                }
            ]);
            res.attachment(nam+'.xlsx');
            return res.send(report);
        })
    })
})

module.exports = router;