//Required dependencies
require('express-router-group')
const express = require('express')
const router = express.Router();

//Plugins initilize
require('dotenv').config()
var cryptorjs = require('cryptorjs');
var myCryptor = new cryptorjs(process.env.CRYPTOR_PASS);

//Models and Controllers
//-----Concepts
const concept = require('./../Modules/SC/Controllers/conceptsController')

//-----Inventory
const {productController} = require('../common/Controllers/inventoryController')

//-----Categories
const cat = require('../Modules/SC/Controllers/categoriesController')

//-----Cellars
const cellar = require('../Modules/SC/Controllers/cellarsController')

//-----Pricelist
const plist = require('../Modules/SC/Controllers/pricelistController')

//-----Measures
const {Measures} = require('../common/Controllers/measureController')

//-----Configuration
const confModel = require('../Modules/SC/Models/configsModel')
const conf = require('./../Modules/SC/Controllers/configsController')

//-----Department
const Depart = require('./../Modules/SC/Controllers/countriesController')

//-----Contact
// const Contact = require('../Modules/SC/Controllers/contactsController')
const {Contacts} = require('../common/Controllers/contactController')

//-----Auditory Logs
const Logs = require('../Modules/SC/Controllers/logsController')

//Routes Groups
//-----Departments Group
router.group('/depart', (router) => {
    router.post('/store', async (req, res) => {
        let data = await Depart.store(req.body);
        if(!('_id' in data)){
            var resp = {result: data, status: "failed"}
        }else {
            var resp = {result: data, status: "success"}
        }
        res.json(resp)
    })
})

//-----Concepts Group
router.group('/concepts', (router) => {
    router.post('/store', async (req, res) => {
        let data = await concept.store(req.body);
        if(!('_id' in data)){
            var resp = {result: data, status: "failed"}
        }else {
            var resp = {result: data, status: "success"}
        }
        res.json(resp)
    })

    router.post('/addsub', async (req, res) => {
        let data = await concept.newSubCat(req.body);
        if(!('_id' in data)){
            var resp = {result: data, status: "failed"}
        }else {
            var resp = {result: data, status: "success"}
        }
        res.json(resp)
    })
})

//-----Contacts Group
router.group('/contact', (router) => {
    router.post('/store', async (req, res) => {
        let data = await Contacts.store(req.body);
        if(!('_id' in data)){
            var resp = {result: data, status: "failed"}
        }else {
            var resp = {result: data, status: "success"}
        }
        res.json(resp)
    })

    router.put('/cancel',async (req,res) => {
        let data = await Contacts.Cancel(req.body);
        if(!('_id' in data)){
            var resp = {result: data, status: "failed"}
        }else {
            var resp = {result: data, status: "success"}
        }
        res.json(resp)
    })
})

//-----Logs Group
router.group('/logs', (router) => {
    router.post('/store', async (req, res) => {
        let data = await Logs.store(req.body);
        if(!('_id' in data)){
            var resp = {result: data, status: "failed"}
        }else {
            var resp = {result: data, status: "success"}
        }
        res.json(resp)
    })
})

 //------Inventory Group
 router.group('/inventory', (router) => {
    router.post('/store',async (req,res) => {
        let data = await productController.store(req.body);
        if(!('_id' in data)){
            var resp = {result: data, status: "failed"}
        }else {
            var resp = {result: myCryptor.encode(data), status: "success"}
        }
        res.json(resp)
    })

    router.put('/addlot',async (req,res) => {
        let data = await productController.addLot(req.body);
        if(!('_id' in data)){
            var resp = {result: data, status: "failed"}
        }else {
            var resp = {result: data, status: "success"}
        }
        res.json(resp)
    })
})

 //------Categories Group
 router.group('/categories', (router) => {
    router.post('/store',async (req,res) => {
        let data = await cat.store(req.body);
        if(!('_id' in data)){
            var resp = {result: data, status: "failed"}
        }else {
            var resp = {result: data, status: "success"}
        }
        res.json(resp)
    })

    router.put('/modcat',async (req,res) => {
        let data = await cat.modCat(req.body);
        if(!('_id' in data)){
            var resp = {result: data, status: "failed"}
        }else {
            var resp = {result: data, status: "success"}
        }
        res.json(resp)
    })
 })
 //-----PriceList Group
router.group('/cellar', (router) => {
    router.post('/store', async (req, res) => {
        let data = await cellar.store(req.body);
        if(!('_id' in data)){
            var resp = {result: data, status: "failed"}//encriptar data
        }else {
            var resp = {result: data, status: "success"}
        }
        res.json(resp)
    })
    router.put('/modcellar',async (req,res) => {
        let data = await cellar.modPlist(req.body);
        if(!('_id' in data)){
            var resp = {result: data, status: "failed"}//encriptar data
        }else {
            var resp = {result: data, status: "success"}
        }
        res.json(resp)
    })
})
 //-----PriceList Group
router.group('/pricelist', (router) => {
    router.post('/store', async (req, res) => {
        let data = await plist.store(req.body);
        if(!('_id' in data)){
            var resp = {result: data, status: "failed"}
        }else {
            var resp = {result: myCryptor.encode(data), status: "success"}
        }
        res.json(resp)
    })
    router.put('/modpl',async (req,res) => {
        let data = await plist.modPlist(req.body);
        if(!('_id' in data)){
            var resp = {result: data, status: "failed"}
        }else {
            var resp = {result: data, status: "success"}
        }
        res.json(resp)
    })
})
//-----Measures Group
router.group('/measures', (router) => {
    router.post('/store', async (req, res) => {
        let data = await Measures.store(req.body);
        if(!('_id' in data)){
            var resp = {result: data, status: "failed"}
        }else {
            var resp = {result: data, status: "success"}
        }
        res.json(resp)
    })
    router.put('/measurement',async (req,res) => {
        let data = await Measures.addUm(req.body);
        if(!('_id' in data)){
            var resp = {result: data, status: "failed"}//encriptar data
        }else {
            var resp = {result: data, status: "success"}
        }
        res.json(resp)
    })
    router.post('/modum',async (req,res) => {
        console.log(req.body)
        let data = await Measures.modUm(req.body);
        if(!('_id' in data)){
            var resp = {result: data, status: "failed"}//encriptar data
        }else {
            var resp = {result: data, status: "success"}
        }
        res.json(resp)
    })
    router.put('/addconvertion',async (req,res) => {
        // console.log(req.body)
        let data = await Measures.pushCum(req.body);
        if(!('_id' in data)){
            var resp = {status: "failed"}
        }else {
            var resp = {status: "success"}
        }
        res.json(resp)
    })
})
 //------Configuration Group
router.group('/configuration', (router) => {
    router.post('/store',async (req,res) => {
        let data = await conf.store(req.body);
        if(!('_id' in data)){
            var resp = {result: data, status: "failed"}
        }else {
            var resp = {result: data, status: "success"}
        }
        res.json(resp)
    })
 })
module.exports = router;