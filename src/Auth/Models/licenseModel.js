const mongoose = require('mongoose');
const { Schema } = mongoose;

const Details = new Schema({
    module: {
        type: String,
        required: true
    },
    items: [String]
}, {_id: false})

const License = new Schema
({
    lic: {
        type: String,
        required: true
    },
    expirationDate: {
        type: Date,
        default: Date.now
    },
    payStatus: {
        type: Boolean,
        default: true
    },
    details: [Details]
})

module.exports = mongoose.model('license', License);