const mongoose = require('mongoose');
const { Schema } = mongoose;
const bcrypt = require('bcrypt')
const LicModel = require('./licenseModel')

const subitemSch = new Schema({
    id: Number,
    url: String,
    text: String,
    actions: [String]
}, {_id: false})

const itemSch = new Schema({
    id: Number,
    icon: String,
    item: String,
    subitems: [subitemSch]
}, {_id: false});

const accessSch = new Schema({
    module: String,
    items: [itemSch]
}, {_id: false});

const Admin = new Schema
({
    name: {
        first: String,
        last: String
    },
    dni: {
        dType: String,
        dni: String
    },
    rol: {
        type: String,
        default: 'Administrador'
    },
    company: {
        _id: {
            type: Schema.Types.ObjectId,
            auto: true,
            index: true
        },
        name: {
            type: String,
            required: true
        },
        nit: {
            type: String,
            required: true
        },
        logo: String,
        license: {
            type: Schema.Types.ObjectId,
            required: true
        },
        rols: [{
            name: {
                type: String,
                required: true
            },
            access: [accessSch]
        }]
        
    },
    birthdate: Date,
    phone: String,
    registerDate: {
        type: Date,
        default: Date.now
    },
    photo: String,
    address: {
        town: {
            type: String,
            required: true
        },
        adInfo: String
    },
    email: String,
    accessData: {
        nickname: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true
        },
        token: {
            type: String,
            required: true
        },
        lastAccess: {
            type: Date,
            default: Date.now
        }
    }
})

Admin.statics.encryptPass = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
}

Admin.methods.validatePass = function (password) {
    return bcrypt.compareSync(password, this.accessData.password)
} 

module.exports = mongoose.model('admin', Admin);