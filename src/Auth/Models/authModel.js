const mongoose = require('mongoose');
const { Schema } = mongoose;
const bcrypt = require('bcrypt')

const Auth = new Schema
({
    name: {
        first: String,
        last: String
    },
    dni: {
        dType: String,
        dni: String
    },
    company: {
        type: Schema.Types.ObjectId,
        required: true
    },
    rol: {
        type: Schema.Types.ObjectId,
        required: true
    },
    birthdate: Date,
    phone: String,
    registerDate: {
        type: Date,
        default: Date.now
    },
    status: {
        type: Boolean,
        default: true
    },
    photo: String,
    address: {
        town: {
            type: String,
            required: true
        },
        adInfo: String
    },
    email: String,
    accessData: {
        nickname: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true
        },
        token: {
            type: String,
            required: true
        },
        lastAccess: {
            type: Date,
            default: Date.now
        }
    }
})

Auth.statics.encryptPass = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
}

Auth.methods.validatePass = function (password) {
    return bcrypt.compareSync(password, this.accessData.password)
} 

module.exports = mongoose.model('auth', Auth);