const User = require('./Models/authModel');
const Admin = require('./Models/adminsModel');
const License = require('./Models/licenseModel');

class table {
    saveLicense(licens) {
        let Data = new License(licens)
        var error = Data.validateSync();
        if (error) 
        {
            // return error;
            let status = [];
            for (const key in error.errors) 
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi, "")
                };
                status.push(res)
            }
            return status;
        } else 
        {
            let status = Data.save();
            return status;
        }
    }
    
    async store(request, type, license) {
        if(type == "admin") {
            let lic = await this.saveLicense(license);
            if(lic.length == null) {
                request.company.license = lic._id
                request.accessData.password = Admin.encryptPass(request.accessData.password)
                let Data = new Admin(request)
                var error = Data.validateSync();
                if (error) 
                {
                    let status = [];
                    for (const key in error.errors) 
                    {
                        let message = error.errors[key].message
                        let res = {
                            field: error.errors[key].path,
                            reason: message.toString().replace(/\\|"/gi, "")
                        };
                        status.push(res)
                    }
                    return status;
                } else 
                {
                    let status = Data.save();
                    return status;
                }
            } 
            else return {status: "failed"}       
        }
        if(type == "user") {
            request.accessData.password = User.encryptPass(request.accessData.password)
            let Data = new User(request)
            var error = Data.validateSync();
            if (error) 
            {
                // return error;
                let status = [];
                for (const key in error.errors) 
                {
                    let message = error.errors[key].message
                    let res = {
                        field: error.errors[key].path,
                        reason: message.toString().replace(/\\|"/gi, "")
                    };
                    status.push(res)
                }
                return status;
            } else 
            {
                let status = Data.save();
                return status;
            }
        }
    }
}

let authController = new table();
module.exports = {authController};