window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

window.$ = window.jQuery = require('jquery');
$('[data-toggle="tooltip"]').tooltip()
// window.dt = require( 'datatables.net' )();

window.Vue = require('vue');
let cryptorjs = require('./Utilities/Plugins/cryptor');
let chidoriAlert = require('./Utilities/Plugins/chidoriAlert');
let swal = require('./Utilities/Plugins/sweetAlert')

import VueRouter from 'vue-router';
import picker from 'vuejs-datepicker';

import vSelect from 'vue-select';
import VModal from 'vue-js-modal'
 
Vue.use(VModal, { dialog: true })

Vue.use(picker);
Vue.use(VueRouter);

//pagination 
import VuePaginate from 'vue-paginate';
Vue.use(VuePaginate); 

//data table UI
//Source
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)
//language
import lang from 'element-ui/lib/locale/lang/es'
import locale from 'element-ui/lib/locale'
const DataTables = require('vue-data-tables') 

locale.use(lang)
Vue.use(DataTables)

import programing from './SF/Components/ProgramingComponent'
import reception from './SF/Components/ReceptionComponent'
import dispensation from './SF/Components/DispensationComponent'
import SFproduct from './SF/Components/ProductComponent'
import SFschedule from './SF/Components/ScheduleComponent'
import SFpatient from './SF/Components/PatientComponent'
import Cont from './SC/Components/ContComponent'
import Inv from './SC/Components/InvComponent'
import Concept from './SC/Components/ConceptComponent'
import InvCat from './SC/Components/CategoryComponent'
import ContList from './SC/Components/ContListComponent'
import JoberList from './SC/Components/WorkerComponent';
import PriceList from './SC/Components/PriceListComponent';
import Measurement from './SC/Components/MeasurementComponent';
import Cellar from './SC/Components/CellarComponent';
import InvList from './SC/Components/InvListComponent';
import SFpatientList from './SF/Components/PatientListComponent'
import SFproductList from './SF/Components/ProductListComponent'
import SFthirds from './SF/Components/ThirdsComponent'
import SFthirdsC from './SF/Components/ThirdsCreateComponent'
import SFreceptionList from './SF/Components/ReceptionListComponent'
import SFreceptionUp from './SF/Components/UpdateReceptionComponent'
import parallax from './SF/Components/ParallaxComponent'
// import SFProgrammingQ from './SF/Components/ProgrammingQuimioComponent';

import SFProgrammingQ from './SF/Components/ProgrammingQuimioComponent';
import SFdispensList from './SF/Components/DispensListComponent'

let paths = {
    reception,
    dispensation,
    programing,   
    Cont,
    Inv,
    SFproduct,
    Concept,
    SFpatient,
    InvCat,
    ContList,
    JoberList,
    PriceList,
    Measurement,
    Cellar,
    InvList,
    SFschedule,
    SFpatientList,
    SFproductList,
    SFthirds,
    SFthirdsC,
    SFdispensList,
    SFproductList,
    SFreceptionList,
    SFreceptionUp,
    parallax,
    // SFProgrammingQ
    SFProgrammingQ
  }
  let scroutes = [ 
    { path: '/vc/sc/cont', component: paths.Cont},
    { path: '/vc/sc/con', component: paths.Concept},
    { path: '/vc/sc/invcat', component: paths.InvCat},
    { path: '/vc/sc/inv', component: paths.Inv},
    { path: '/vc/sc/contlist', component: paths.ContList},
    { path: '/vc/sc/employeelist', component: paths.JoberList},
    { path: '/vc/sc/pricelist', component: paths.PriceList},
    { path: '/vc/sc/measure', component: paths.Measurement},
    { path: '/vc/sc/cellar', component: paths.Cellar},
    { path: '/vc/sc/invlist', component: paths.InvList}
  ]

  let sfroutes = [
    { path: '/vc/sf/reception', component: paths.reception},
    { path: '/vc/sf/dispensation', component: paths.dispensation},    
    { path: '/vc/sf/patient', component: paths.SFpatient},
    { path: '/vc/sf/patient-programing', component: paths.programing},
    { path: '/vc/sf/query-schedule', component: paths.SFschedule},
    { path: '/vc/sf/product', component: paths.SFproduct},
    { path: '/vc/sf/product-list', component: paths.SFproductList},
    { path: '/vc/sf/patient-list', component: paths.SFpatientList},
    { path: '/vc/sf/thirds-list', component: paths.SFthirds},
    { path: '/vc/sf/thirds-create', component: paths.SFthirdsC},
    { path: '/vc/sf/dispens-list', component: paths.SFdispensList},
    { path: '/vc/sf/patient-list', component: paths.SFpatientList},
    { path: '/vc/sf/receptions-list', component: paths.SFreceptionList},
    { path: '/', component: paths.parallax },
    { path: '/vc/sf/update/reception/:id?', component: paths.SFreceptionUp},
    // { path: '/vc/sf/programming-quimio', component: paths.SFProgrammingQ}
    { path: '/vc/sf/programming-quimio', component: paths.SFProgrammingQ}
  ]
  
  const scrouter = new VueRouter({
    uid: 'sc',
    mode: 'history',
    routes: scroutes // short for `routes: routes`
  })

  const sfrouter = new VueRouter({
    uid: 'sf',
    // mode: 'history',   
    routes: sfroutes // short for `routes: routes`
  })

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('sc-component', require('./SC/Components/MainComponent.vue').default);
Vue.component('sf-component', require('./SF/Components/MainComponent.vue').default);
Vue.component('view-more-disp', require('./SF/Components/DispenViewComponent.vue').default);
Vue.component('login-component', require('../common/Components/LoginComponent.vue').default);
Vue.component('progprint-component', require('./SF/Components/PrintProgComponent.vue').default);
Vue.component('v-select', vSelect)

const mainapp = new Vue({
    el: '#main'
});
const app = new Vue({
    el: '#sc',
    router: scrouter 
});

const oapp = new Vue({
    el: '#sf',
    router: sfrouter
});

const print = new Vue({
  el: '#progprint',
  router: sfrouter
});

