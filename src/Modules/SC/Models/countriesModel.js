const mongoose = require('mongoose')
const { Schema } = mongoose

const Towns = new Schema({
    name: {
        type: String,
        required: true
    },
    postalCode: {
        type: Number,
        required: true
    }
})

const Countries = new Schema({
    name: {
        type: String,
        required: true
    },
    towns: [Towns]
})

module.exports = mongoose.model('Countries', Countries)