const mongoose = require('mongoose')
const { Schema } = mongoose

const PriceList = new Schema({
    company:{
        type:String,
        required:true
    },
    name: {
        type: String,
        required: true
    },
    status:{
        type: Boolean,
        required :true

    }
})

module.exports = mongoose.model('Pricelists', PriceList)