const mongoose = require('mongoose')
const { Schema } = mongoose

let Contact = new Schema({
    firstname: {
        first: String,
        second: String
    },
    lastname:{
        first: String,
        second: String
    },
    dni: {
        dni: {
            type: String,
            required: true
        },
        dv: {
            type: String,
            required: false
        },
        dniFormat: {
            type: String,
            enum: ['NIT', 'CC', 'CE', 'PP', 'TE' , 'RC', 'TI'],
            required: true
        }
    },
    contactType: {
        type: [String],
        required: true
    },

    phone: [],
    email: String,
    company: String,
    address: [{
        town: {type: String, required: true},
        dep: {type: String, required: true},
        aditionalInf: {type: String, required: true},
        establishment:{type: String, required: true}
    }],
    payConditions: {
        discount: {type: Number},
        payModality: {type: String, required:true},
        credit: {type: String, required:false},
        deadlines: {type: String, required: false}
    },
    tributaryInf: {
        regimen: String,
        contributingCategory: String,
        typePerson: String
        
    },
    worker:{
        jobTitle: String,
        costCenter: String,
        seller: String,
        numContract: String,
        typeContract: String,
        dateIniContract: Date,
        dateEndContract: Date
    },
    registerDate: {
        type: Date,
        default: Date.now
    }
})

let Contacts = new Schema({
    nit: {
        type: String,
        required: true
    },
    module: {
        type: String,
        enum: ['sc', 'sf'],
        required: true
    },
    contact: Contact,
    slug: {
        type: Schema.Types.ObjectId,
        auto: true,
        index: true,
        required: true
    }
})

module.exports = mongoose.model('Contacts', Contacts)