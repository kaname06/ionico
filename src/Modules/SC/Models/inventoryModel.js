const mongoose = require('mongoose');
const { Schema } = mongoose

let Item = new Schema({
    code: {
        barcode: {
            type: Number,
            required:false
        },
        consecutive: {
            type: String,
            required: true,
            unique:true
        }
    },
    description: {
        type: String,
        required: true
    },

    itemType: {
        type: String,
        required: true
    },
    unit:{
        type: String,
        required: false
    },
    lot:[{
        stock: {
            type: Number,
            required: true
                },
        provider: {
            type: String,
            required: false
        },
        expirationDate:{
            type: Date,
            default: null
        },
        prices:{ 
            sale:{
                type: Number,
                required: true
            },
            purchase: {
                type: Number,
                required: true
            },
            pricelist: [
                {
                    name: String,
                    val: Number
                }
            ],
            utlity: {
                type: Number,
                default: 0
            } 
        },
        tax: {
            type: Number,
            required: false,
            default: 0
        }        
    }],
    status: {
        type: Boolean,
        required: false
    },
    category: {
        // type: Schema.Types.ObjectId,
        type: String,
        required: true
    }
},{ _id : false })

let Inventory = new Schema({
    nit: {
        type: String,
        required: true
    },
    module: {
        type: String,
        required: true
    },
    item: Item
})

module.exports = mongoose.model('Inventory',Inventory)