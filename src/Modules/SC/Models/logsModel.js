const mongoose = require('mongoose')
const { Schema } = mongoose

const Logs = new Schema({
    actionType: {
        type: String,
        required: true
    },
    actionDate: {
        type: Date,
        default: Date.now
    },
    actionInfo: {
        description: {
            type: String,
            required: true
        },
        monto: {
            type: Number,
            default: 0
        }
    },
    user: {
        user_nick: {
            type: String,
            required: true
        },
        public_ip: {
            type: String,
            required: true
        },
        terminal_mac: {
            type: String,
            required: true
        }
    }
})

module.exports = mongoose.model('Logs', Logs)