const mongoose = require('mongoose')
const { Schema } = mongoose

const Subconcept = new Schema({
    name: {
        type: String,
        required: true
    },
    value: {
        type: Number,
        required: function () {
            return this.name == "nomina";
        }
    }
},{ _id : false })

const Concepts = new Schema({
    name: {
        type: String,
        required: [true, "This field is required"]
    },
    subConcepts: [Subconcept],
    company:String
})

module.exports = mongoose.model('Concepts', Concepts)