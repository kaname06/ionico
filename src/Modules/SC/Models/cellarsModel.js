const mongoose = require('mongoose')
const { Schema } = mongoose

const Cellars = new Schema({
    company:{
        type:String,
        required:true
    },
    name: {
        type: String,
        required: true
    },
    location: String,
    description: String,
    status:{
        type:Boolean,
        default:true
    },
    storage: [{
        product: Schema.Types.ObjectId,
        amount: Number,
        measure: String
    }]   
}) 

module.exports = mongoose.model('cellars',Cellars)