const mongoose = require('mongoose')
const { Schema } = mongoose

const Worker = new Schema({
    name: {
        first: {
            type: String,
            required: true
        },
        last: {
            type: String,
            required: true
        }
    },
    dni: {
        dni: {
            type: Number,
            required: true
        },
        dniFormat: {
            type: String,
            enum: ['C.C', 'C.E'],
            required: true
        }
    },
    area: String,
    class: String,//Por Determinar
    charge: String,
    bankData: {
        entity: String,
        account: String
    },
    RL:{
        name: String,
        percent: Number
    },
    company: {
        name: String,
        subsidiary: String
    },
    contact: {
        contractType: {type: String, required: true},
        startDate: {type: Date, required: true},
        finishDate: Date
    },
    healthProvider: String
})


module.exports = mongoose.model('Workers', Worker)