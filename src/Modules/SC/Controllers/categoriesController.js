const Category = require('../Models/categoriesModel')

class categories {
     store(request) {
        let cat = new Category(request)
        let error = cat.validateSync();
        if(error){
            var fields = []
            for (const key in error.errors) {
                    let result = {field: error.errors[key].path, feedback: error.errors[key].message.replace(/\\|"/gi, "")}
                    fields.push(result);
                    result = null
            }
            return fields;
        }else {
            let Status = cat.save()
            return Status;
        }
    }

    async modCat(request) {
        let dataa = await Category.findOne({ _id: request.id,company: request.nit })
        if(dataa != null)
        {
            dataa.name = request.data
            let error = dataa.validateSync()
            if(error)
            {
                var fields = []
                for (const key in error.errors) {
                    let result = {field: error.errors[key].path, feedback: error.errors[key].message.replace(/\\|"/gi,"")}
                    fields.push(result);
                    result = null
                }
                return fields
            }
            else{
                let reg = dataa.save()
                return reg
            }
        }else
        {
            return 'Failed, document not found'
        }
    }
}

module.exports = new categories();