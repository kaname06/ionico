const Configuration = require('../Models/configsModel');

class configuration {
    store(request){

        let conf = new Configuration(request)
        let error = conf.validateSync();
        if(error){

            var fields = []
            for (const key in error.errors) {
                let result = {field: key, feedback: error.errors[key].message.replace(/\\|"/gi,"")}
                fields.push(result);
                result = null
            }
            return fields
        }else{
            let data = conf.save()
            return data
        }
    }
}
module.exports = new configuration();