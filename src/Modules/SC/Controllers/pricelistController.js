const PriceList = require('../Models/pricelistModel')

class pricelist {
     store(request) {
        let pl = new PriceList(request)
        let error = pl.validateSync();
        if(error){
            var fields = []
            for (const key in error.errors) {
                    let result = {field: error.errors[key].path, feedback: error.errors[key].message.replace(/\\|"/gi, "")}
                    fields.push(result);
                    result = null
            }
            return fields;
        }else {
            let Status = pl.save()
            return Status;
        }
    }

    async modPlist(request) {
        let dataa = await PriceList.findOne({ _id: request.id, company: request.nit })
        if(dataa != null)
        {
            dataa.name = request.data
            let error = dataa.validateSync()
            if(error)
            {
                var fields = []
                for (const key in error.errors) {
                    let result = {field: error.errors[key].path, feedback: error.errors[key].message.replace(/\\|"/gi,"")}
                    fields.push(result);
                    result = null
                }
                return fields
            }
            else{
                let reg = dataa.save()
                return reg
            }
        }else
        {
            return 'Failed, document not found'
        }
    }
}

module.exports = new pricelist();