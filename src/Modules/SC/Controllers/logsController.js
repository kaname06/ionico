const Logs = require('../Models/logsModel')

class log {
     store(request) {
        let logs = new Logs(request)
        let error = logs.validateSync();
        if(error){
            var fields = []
            for (const key in error.errors) {
                    let result = {field: error.errors[key].path, feedback: error.errors[key].message.replace(/\\|"/gi, "")}
                    fields.push(result);
                    result = null
            }
            return fields;
        }else {
            let Status = logs.save()
            // console.log("Response: ", Status)
            return Status;
        }
    }
}

module.exports = new log();