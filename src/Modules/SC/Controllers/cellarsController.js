const Cellars = require('../Models/cellarsModel')

class cellars {
    store(request) {
        let bod = new Cellars(request)
        let error = bod.validateSync()
        if(error){
            var fields = []
            for (const key in error.errors) {
                let result = {fields: error.errors[key].path, feedback: error.errors[key].message.replace(/\\|"/gi, "")}
                fields.push(result)
                result = null
            }
            return fields;
        }else {
            let status = bod.save()
            return status
        }
    }

}
module.exports = new cellars();