const Concept = require('../Models/conceptsModel')

class concepts {
     store(request) {
        let concep = new Concept(request)
        let error = concep.validateSync();
        if(error){
            var fields = []
            for (const key in error.errors) {
                    let result = {field: error.errors[key].path, feedback: error.errors[key].message.replace(/\\|"/gi, "")}
                    fields.push(result);
                    result = null
            }
            return fields;
        }else {
            let Status = concep.save()
            return Status;
        }
    }

    async newSubCat(request) {
        let dataa = await Concept.findOne({ _id: request.id })
        if(dataa != null)
        {
            dataa.subConcepts.push(request.data)
            let error = dataa.validateSync()
            if(error)
            {
                var fields = []
                for (const key in error.errors) {
                    let result = {field: error.errors[key].path, feedback: error.errors[key].message.replace(/\\|"/gi,"")}
                    fields.push(result);
                    result = null
                }
                return fields
            }
            else{
                let reg = dataa.save()
                return reg
            }
        }else
        {
            return 'Failed, document not found'
        }
    }
}

module.exports = new concepts();