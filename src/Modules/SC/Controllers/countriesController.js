const Depart = require('../Models/countriesModel')

class department {
     store(request) {
        let depart = new Depart({
            name: request.name,
            towns: request.towns
        })
        let error = depart.validateSync();
        if(error){
            var fields = []
            for (const key in error.errors) {
                    let result = {field: error.errors[key].path, feedback: error.errors[key].message.replace(/\\|"/gi, "")}
                    fields.push(result);
                    result = null
            }
            return fields
        }else {
            let data = depart.save()
            // console.log("Response: ", Status)
            // return {result: data, status: "success"};
            return data
        }
    }
}

module.exports = new department();