const Inventory = require('../Models/inventoryModel')

class inventory {
    store(request){
        let inv = new Inventory(request)
        
        let error = inv.validateSync();
        if(error){

            var fields = []
            for (const key in error.errors) {
                let result = {field: error.errors[key].path, feedback: error.errors[key].message.replace(/\\|"/gi,"")}
                fields.push(result);
                result = null
            }
            return fields
        }else{
            let data = inv.save()
            return data
        }

    }

    async addLot(request) {
        let data = await Inventory.findOne({_id: request.id, nit: request.nit})
        if(data != null)
        {
            data.item.lot.push(request.data)
            let error = data.validateSync()
            if(error)
            {
                var fields = []
                for (const key in error.errors) {
                    let result = {field: error.errors[key].path, feedback: error.errors[key].message.replace(/\\|"/gi,"")}
                    fields.push(result);
                    result = null
                }
                return fields
            }
            else{
                let reg = data.save()
                return reg
            }
        }else
        {
            return 'Failed, document not found'
        }
    }

}

module.exports = new inventory();