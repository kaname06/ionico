const chidoriAlert = require('../ModelsPlugin/chidoriAlert')

const alertPlugin = {}

alertPlugin.install = function (Vue) {
    Vue.prototype.$chidoriAlert = chidoriAlert
}

Vue.use(alertPlugin)