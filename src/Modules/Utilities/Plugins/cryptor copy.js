const { AES, enc } = require('crypto-ts');
var cryptSecret = 'kaiokenx200alafergax100alafergax50alaferga';

const encode = function (data)
{
    return AES.encrypt(JSON.stringify(data), cryptSecret).toString();
}

const decode = function(data)
{
    return JSON.parse(AES.decrypt(data.toString(), cryptSecret).toString(enc.Utf8))
}

module.exports = {encode, decode}