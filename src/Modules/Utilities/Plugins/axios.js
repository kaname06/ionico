const axios = require('axios')

// axios.defaults.baseURL = 'http://localhost:3010/'

const getCookie = name => {
  let v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)')
  return v ? v[2] : null
}

const header = (method, url, isAuth, data, toSendAFile, _module) => 
{
    let options = { method }
  
    if(_module == 'PSF')
    {
      axios.defaults.baseURL = 'http://localhost:3001/'
    } else if(_module == 'SF')
    {
      axios.defaults.baseURL = 'http://localhost:3000/'
    } else if(_module == 'GD')
    {
      axios.defaults.baseURL = 'http://localhost:3010/'
    }

    options.url = url    
    data && (options.data = data)
    options.headers = {'Content-Type': toSendAFile ? 'multipart/form-data' : 'application/json' }
  
    if (typeof isAuth !== 'boolean' || !isAuth) 
    {            
      options.headers = {...options.headers, 'x-m-i': (_module == 'PSF' || _module == 'SF') ? 'SF' : 'GD'}      
    } else if (isAuth) {
      const token = document && getCookie('token')
      token && (options.headers = {...options.headers, 'x-a-t': `${token}`, 'x-m-i': (_module == 'PSF' || _module == 'SF') ? `SF` : 'GD'})
    }
    return options
}
  
export default (method, url, isAuth = false, data = null, toSendAFile = false, _module = null) =>
new Promise((resolve, reject) => {
    axios(header(method, url, isAuth, data, toSendAFile, _module))
    .then(res => resolve(res))
    .catch(err => reject(err))
})
  