const mongoose = require('mongoose');
var utc = new Date();
utc.setHours(utc.getHours() - 5);	
const { Schema } = mongoose;
// required: [true, 'messagge']
// type: 0 = EPS, 1 = PROVIDER, 2 = DEVOLUCION
const Reception = new Schema(
{
	type_reception: 
	{
		type: String
	},
	status: {type: String},
	provider: {type: Schema.Types.ObjectId},
	patient: {type: Schema.Types.ObjectId},
	bill_number: {type: String},
	// bill_value: {type: Number},
	eps: Schema.Types.ObjectId,	
	services: {type: String},	
	date_creation: { type: Date, default: utc },	
	// {
	// 	packaging: 
	// 	{ 
	// 		status: 
	// 		{
	// 			type: Boolean,
	// 			default: false
	// 		},
	// 		type_r: Number	
	// 	},
	// 	cold_chain: 
	// 	{
	// 		status: 
	// 		{
	// 			type: Boolean,
	// 			default: false
	// 		},
	// 		type_r: Number
	// 	},
	// 	packaging_container: 
	// 	{
	// 		status: 
	// 		{
	// 			type: Boolean,
	// 			default: false
	// 		},
	// 		type_r: Number,			
	// 	},
	// 	appereance: 
	// 	{			
	// 		status: 
	// 		{
	// 			type: Boolean,
	// 			default: false
	// 		}
	// 	},
	// 	smell: 
	// 	{
	// 		status: 
	// 		{
	// 			type: Boolean,
	// 			default: false
	// 		},
	// 		type_r: Number			
	// 	},
	// 	requested_product: 
	// 	{
	// 		status: 
	// 		{
	// 			type: Boolean,
	// 			default: false
	// 		},
	// 		type_r: Number
	// 	},
	// 	quantity_rotation: 
	// 	{
	// 		status: 
	// 		{
	// 			type: Boolean,
	// 			default: false
	// 		},
	// 		type_r: Number
	// 	},
	// 	product_inventory: 
	// 	{
	// 		status: 
	// 		{
	// 			type: Boolean,
	// 			default: false
	// 		},
	// 		type_r: Number
	// 	},
	// 	billed_value: 
	// 	{
	// 		status: 
	// 		{
	// 			type: Boolean,
	// 			default: false
	// 		},
	// 		type_r: Number
	// 	},
	// 	expiration_date_one: 
	// 	{
	// 		status: 
	// 		{
	// 			type: Boolean,
	// 			default: false
	// 		},
	// 		type_r: Number
	// 	},
	// 	product_necessary: 
	// 	{
	// 		status: 
	// 		{
	// 			type: Boolean,
	// 			default: false
	// 		},
	// 		type_r: Number
	// 	},
	// 	checked: Boolean
	// },
	responsible_dni: String,
	products:
	[
		{
			product: String,
			lote: String,
			status: Boolean,
			observation: String,
			quantity: Number,
			approved_dir: {type: Boolean, default: false},
			expiration_date: String,
			status_details: {},
		}
	]
})

module.exports = mongoose.model('Reception', Reception)