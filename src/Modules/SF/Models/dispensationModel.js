const mongoose = require('mongoose');
const { Schema } = mongoose;

const Dispensation = new Schema(
{
    dispType: {type: String, required: true},
    receptor: {
        patientDni: {type: String},
        service: {type: String},
        name: {type: String, required: true}
    },
    products: [{
        productId: {type: Schema.Types.ObjectId, required: true},
        lote: {type: Schema.Types.ObjectId, required: true},
        expirationDate: {type: Date, default: null},
        volume: {type: String},
        quantity: {type: Number, required: true}
    }],
    dispDate: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Dispensation', Dispensation)