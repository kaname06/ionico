const mongoose = require('mongoose');
const { Schema } = mongoose;

const DynamicTable = new Schema
({
    contactType:{type: String, required: true},
    adminType: String,
    code: String,    
    nitClient: {
        type: String,
        required: true
    },
    name: {
        first: String,        
    },
    status: {
        type: Boolean,
        default: true
    },    
    address: [{
        department: String,
        town: String,
        adInfo: String
    }],
    phones: [String],
    contact_admin: String        
})
module.exports = mongoose.model('Dynamic_Table', DynamicTable)
