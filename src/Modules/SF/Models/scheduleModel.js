const mongoose = require('mongoose');
const { Schema } = mongoose;

var utc = new Date();
utc.setHours(utc.getHours() - 5);	

const Schedule = new Schema
({
    patient: {
        type: Schema.Types.ObjectId,
        required: true
    },
    schemaN: {
        type: String,
        required: true
    },
    meetings: [
        {
            date: Date,
            hour: String,
            module: String,
            note: String,
            iop: String
        }
    ],
    drugs: [Schema.Types.ObjectId],
    status: {
        type: Boolean,
        default: false
    },
    // condition: {
    //     type: String,
    //     required: true
    // },
    cie10Code: {
        type: String,
        required: true
    },
    autorization: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: utc
    }
})

module.exports = mongoose.model('Schedule', Schedule);