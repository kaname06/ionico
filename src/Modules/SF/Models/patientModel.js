const mongoose = require('mongoose');
const { Schema } = mongoose;

const Patient = new Schema
({
    dni: 
    {
        tipo: String,
        num: 
        {
            type: Number,
            required: true,
            unique: true
        }
    },
    basic_info:
    {
        name:
        {
            fname: String,
            sname: String
        },
        lastname:
        {
            flname: String,
            slname: String,
        },
        birthdate: String,
        rh: String,
        eps: Schema.Types.ObjectId,
        gender: String,
        location:{ 
            town: String,
            dep: String,
            address: String
        },
        civil_state: String,
        phone: String,
        work: String,
        vinculation: String,
        live: {type: Boolean, default: true}
    }
})

module.exports = mongoose.model('Patient', Patient);