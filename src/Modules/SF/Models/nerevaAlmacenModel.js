const mongoose = require('mongoose')
const { Schema } = mongoose

 const neveraAlmacenTable = new Schema
 ({
    name:
    {
        type: String,
        required: true
    },
    value:
    {
        type: Number,
        required: true        
    },
    storage_conditions:
    [{
        creation_date: 
        {
            type: Date,
            default: Date.now
        },
        date:
        {
            type: Date,
        },
        room_temperature:
        {
            type: Number
        },
        rh:
        {
            type: Number
        },
        id_responsible: 
        {
            type: String
        }
    }],
    fridge:
    [{
        name:
        {
            type: String
        },
        tempetature:
        {
            type: Number
        }
    }],
    tracing:
    [{
        action:
        {
            type: String
        },
        tracing_date:
        {
            type: Date
        },
        id_responsible:
        {
            type: String
        },
        request_status:
        {
            type: Boolean
        },
        solution:
        {
            type: String
        }
    }]
 })

 module.exports = mongoose.model('nevera_almacen', neveraAlmacenTable);