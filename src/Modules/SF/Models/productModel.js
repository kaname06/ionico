const mongoose = require('mongoose');
const { Schema } = mongoose;

const Product = new Schema(
{
    name: {type: String, required: true},
    consecutive: {type: String, required: true},
    lotes:[
    {
        // id: {type: String, unique: true},
        stock: 
        {
            type: Number,
            validate: 
            {
                validator: Number.isInteger,
                message: '{VALUE} is not an integer value'
            }
        },
        lote: String,
        inv: {
            type: String,
            enum: ['EPS', 'CRLGC']
        },
        expiration_date:
        {
            type: String
        }
    }],
    type: String,
    lab: Schema.Types.ObjectId,
    bar_code: String,
    farmaceutic_form: String,
    invima: String,
    min_stock: Number,
    concentration: String,
    point_order: Number,
    presentation: String,
    provider: Schema.Types.ObjectId,
    measure: String,
    code_cum: {type: String, default: 'No aplica'},
    category: String,
    status: {type: Boolean, default: true}
});


module.exports = mongoose.model('Product', Product)