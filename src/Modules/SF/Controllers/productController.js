const ProductModel = require('../Models/productModel')

class table 
{
    store(request) 
    {
        let Data = new ProductModel(request)
        var error = Data.validateSync();
        if (error) {
            let status = [];
            for (const key in error.errors) {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi, "")
                };
                status.push(res)
            }
            return status;
        } else {
            let status = Data.save();
            return status;
        }
    }

    async restingUnit(productId, lote, units) {
        let data = await ProductModel.findById(productId);
        let lotes = data.lotes
        for (const key in lotes) {
            if(lotes[key]._id == lote.toString()) {
                data.lotes[key].stock = data.lotes[key].stock - units
                break;
            }
        }
        let result = await data.save()
        return result
    }

    async addLote(request, type) {
        let saved = []
        for (const key in request) 
        {
            let data = await ProductModel.findById(request[key].product);
            if(request[key].approved_dir)
            {
                let inv = ''
                if(type == 'EPS')
                {
                    inv = 'EPS'
                } else {
                    inv = 'CRLGC'
                }
                let lote = {
                    lote: request[key].lote,
                    expiration_date: request[key].expiration_date,
                    stock: request[key].quantity,
                    inv: inv
                }
                data.lotes.push(lote)
                let doit = await data.save()
                console.log(doit)
                saved.push(doit._id)
            }
        }      
        let res = {
            save: saved,
            status: 'success'
        }
        return res;
    }

    async verifyStock(id, lote) {
        let data = await ProductModel.findById(id);
        if(data != null) {
            for (const key in data.lotes) {
                if(data.lotes[key]._id == lote) {
                    if(data.lotes[key].stock <= 0) {
                        return true
                    }
                    break;
                }
            }
        }
    }
}
let prodController = new table();
module.exports = {prodController};