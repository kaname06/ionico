const Schedule = require('../Models/scheduleModel')

class table 
{
    store(request) 
    {
        let Data = new Schedule(request)        
        var error = Data.validateSync();
        if (error) 
        {
            let status = [];
            for (const key in error.errors) 
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi, "")
                };
                status.push(res)
            }
            return status;
        } else 
        {
            let status = Data.save();
            return status;
        }
    }

    update(request)
    {
        let data = Schedule.findByIdAndUpdate(request.id, request.data)
        return data
    }

    async deleteItem(id, target) {     
        let data = await Schedule.findById(id)
        data.meetings.splice(target, 1)
        let res = await data.save()
        return res
    }
}
let scheduleController = new table();
module.exports = {scheduleController};