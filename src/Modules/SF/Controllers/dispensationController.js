const DispModel = require('../Models/dispensationModel')
const ReceptionModel = require('../Models/receptionModel');
const { prodController } = require('./productController');
const {receptionController} = require('./receptionController');

class table 
{
    store(request) 
    {
        let errcounter = 0
        let errors = []
        let ins = []
        for (const key in request) {
            let Data = new DispModel(request[key])        
            var error = Data.validateSync();
            if (error) 
            {
                let status = [];
                for (const key in error.errors) 
                {
                    let message = error.errors[key].message
                    let res = {
                        field: error.errors[key].path,
                        reason: message.toString().replace(/\\|"/gi, "")
                    };
                    status.push(res)
                }
                errors.push(status);
                errcounter = errcounter + 1;
            } else 
            {
                let status = Data.save();
                let products = request[key].products
                let product;
                for (const key in products) {
                    product = products[key]
                    prodController.restingUnit(product.productId, product.lote, product.quantity)
                    receptionController.verifyForDones(product.asocRcp)
                }   
                ins.push(status)
            }
            Data = null
        }
        if(errcounter > 0 || errors.length > 0)
            return {err: errcounter, errors: errors}
        else
            return {_id: 0, data: ins}
    }

     async verifyRecep(id) {
        let lava = await ReceptionModel.find({'patient': id})
        let r = null
        if(lava.length > 0) {
             r = {data: lava, status: true}            
        }
        else {
            r = {status: false}
        }

        return r
    }
}
let dispenController = new table();
module.exports = {dispenController};