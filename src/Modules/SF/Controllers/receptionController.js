const ReceptionModel = require('../Models/receptionModel');
const { prodController } = require('./productController');

class table
{	
	store(request)
	{		
		let Data = new ReceptionModel(request)		
		var error = Data.validateSync();		
		if(error)
		{			
			let status = [];			
			for(const key in error.errors)
			{			
				let message = error.errors[key].message
				let res = {
					field: error.errors[key].path,
					reason: message.toString().replace(/\\|"/gi, "")
				};
				status.push(res)
			}
			return status;
		}
		else
		{				
			let status = Data.save();
			return status;
		}	    
	    // .then(() => {return({status:'sepe'})}).catch((err)=>console.error(err))
	}
	async approvedProducts(id, data, who)
	{
		if(who == 'DT')
		{
			let a = await ReceptionModel.findByIdAndUpdate(id, data)			
			if('_id' in a)
			{				
				let regProd = await prodController.addLote(data.products, data.type_reception)
				console.log(regProd)
				return regProd;
			}	
			else
			{
				return 'error grave';
			}		
		}				
	}
	async verifyForDones(recepId) {
		let a = await ReceptionModel.findById(recepId);
		if(a != null) {
			dones = 0
			let rest = null
			for (const key in a.products) {
				let result = prodController.verifyStock(a.products[key].product, a.products[key].lote)
				if(result)
					dones++
			}
			if(dones == a.products.length) {
				a.status = 'DONE'
				rest = await a.save()
			}
			return rest;
		}
	}
}
let receptionController = new table();
module.exports = {receptionController};