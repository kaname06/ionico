const PatientModel = require('../Models/patientModel')

class table
{
    store(request)
    {
        let Data = new PatientModel(request)        
        var error = Data.validateSync();
        if (error) 
        {
            let status = [];
            for (const key in error.errors) 
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi, "")
                };
                status.push(res)
            }
            return status;
        } else 
        {
            let status = Data.save();
            return status;
        }
    }
}

let patientController = new table();
module.exports = {patientController};
