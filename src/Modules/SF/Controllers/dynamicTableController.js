const DynamicTableModel = require('../Models/dynamicTableModel')

class table
{
    store(request)
    {
        let Data = new DynamicTableModel(request)
        var error = Data.validateSync();
        if (error) 
        {
            // return error;
            let status = [];
            for (const key in error.errors) 
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi, "")
                };
                status.push(res)
            }
            return status;
        } else 
        {
            let status = Data.save();
            return status;
        }
    }    
    async update(id, datas)
    {
        let res = []        
        let data = await DynamicTableModel.findByIdAndUpdate(id, datas)        
        if ('_id' in data) 
        {            
            res = {
                status: 'success',
                data: data
            }
        } else {
            res = {
                status: 'error'
            }
        }
        return res;
    }
    async del(id)
    {
        let res = []        
        let data = await DynamicTableModel.findOneAndDelete({_id: id})        
        if ('_id' in data) 
        {            
            res = {
                status: 'success',
                data: data
            }
        } else {
            res = {
                status: 'error'
            }
        }
        return res;
    }
}
let ContactsController = new table();
module.exports = {ContactsController};