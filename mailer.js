//---------------Mailer
var nodemailer = require('nodemailer');
var user = {
    name = process.env.SUPPORT_EMAIL_USER,
    pass = process.env.SUPPORT_EMAIL_PASS
}
var transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: user.name,
    pass: user.pass
  },
  tls: {
      rejectUnauthorized: false
  }
});

var html = "<h1>Header con HTML</h1>"
html += "<p style='color: #cecece'; font-size: 55px;>Bienvenido al servicio de correspondencia y soporte de IONICO</p>"
html += "<p>Gracias por usar nuestros servicios :)</p>"

var mailOptions = {
  from: user.name,
  to: 'adriandavidcorreamass@gmail.com',
  subject: 'Sending Email using Node.js',
  html
};

transporter.sendMail(mailOptions, function(error, info){
  if (error) {
    console.log('Mail error: ' + error);
  } else {
    console.log('Email sent: ' + info.response);
  }
});
//---------------End Mailer